﻿using System;
using System.Windows;
using System.Windows.Documents;
using TASKer.Logic.Mail;
using TASKer.Util;

namespace TASKer.Dialogs
{
	/// <summary>
	/// Interaction logic for NewMessage.xaml
	/// 
	/// A dialog for composing a mail message.
	/// </summary>
	public partial class MessageComposeDialog : RefreshingDialog
	{
		//Whether to send right now or wait.
		private bool scheduleSend;

		/// <summary>
		/// Create a message compose dialog.
		/// </summary>
		/// <param name="refresh">Optional method to call when done to update UI.</param>
		public MessageComposeDialog(RefreshTasksMethod refresh = null) : base(refresh)
		{
			InitializeComponent();

			scheduleSend = false;
			ScheduleSend.IsChecked = false;
			SendNow.IsChecked = true;
		}

		/// <summary>
		/// Called when either one of the schedule options is checked.
		/// Updates the state depending on which one was clicked.
		/// </summary>
		private void ScheduleSend_Checked(object sender, RoutedEventArgs e)
		{
			if (sender == ScheduleSend)
			{
				scheduleSend = true;
			}
			if (sender == SendNow)
			{
				scheduleSend = false;
			}
		}

		/// <summary>
		/// Called when the user presses confirm.
		/// Sends the message or schedules it to be sent at the given time.
		/// </summary>
		private void ConfirmButton_Click(object sender, RoutedEventArgs e)
		{
			string bodyText = new TextRange(Body.Document.ContentStart, Body.Document.ContentEnd).Text;
			//Validate
			if(string.IsNullOrWhiteSpace(bodyText))
			{
				MessageBox.Show("Please fill a message body");
				return;
			}
			if(string.IsNullOrWhiteSpace(To.Text))
			{
				MessageBox.Show("Please fill a receipant");
				return;
			}

			MailMessage message = new MailMessage(Subject.Text, GmailUserInfo.Current.Address, DateTime.Now, bodyText);

			if (scheduleSend)
			{
				//Validate
				if(SendDate.SelectedDate == null || SendTime.Time == null)
				{
					MessageBox.Show("Please fill a schedule time");
					return;
				}

				//Schedule
				Schedule.ScheduleJob((p) => { ConfirmButton.Dispatcher.Invoke(() => { message.Send(To.Text); }); }, (DateTime)SendDate.SelectedDate + SendTime.Time);
			}
			else
			{
				message.Send(To.Text);
			}
			Close();
		}
	}
}
