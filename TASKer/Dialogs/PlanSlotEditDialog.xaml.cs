﻿using System;
using System.Linq;
using System.Windows;
using TASKer.Logic;
using TASKer.Logic.APIs;
using TASKer.Logic.Database;
using TASKer.Logic.Plan;

namespace TASKer.Dialogs
{
	/// <summary>
	/// Interaction logic for NewPlanSlot.xaml
	/// 
	/// A dialog for editing/creating a plan slot for a week plan
	/// </summary>
	public partial class PlanSlotEditDialog : RefreshingDialog
	{
		//Optional planslot to edit instead of create new.
		private PlanSlot edit;

		/// <summary>
		/// Create a new plan slot edit dialog
		/// </summary>
		/// <param name="refresh">Optional method to call when the dialog is completed to refresh UI</param>
		/// <param name="slot">Optional existing slot to edit</param>
		public PlanSlotEditDialog(RefreshTasksMethod refresh = null, PlanSlot slot = null) : base(refresh)
		{
			InitializeComponent();
			RepeatOptions.ItemsSource = Enum.GetValues(typeof(RepeatType));

			edit = slot;

			if (slot != null)
			{
				if(slot.Title != "New Slot") SlotTitle.Text = slot.Title;

				StartTime.Time = slot.StartTime;
				EndTime.Time = slot.EndTime;

				SlotDate.SelectedDate = slot.StartDate;
				RepeatOptions.SelectedItem = edit.Repeat;

				if (edit.UserTask != null)
					SelectedTask.SelectedItem = edit.UserTask.Name;
			}

			LoadTasks();

		}

		/// <summary>
		/// Loads the existing tasks for the task selction combobox.
		/// Called in the beggining and any time a task is added using the New Task button.
		/// </summary>
		private void LoadTasks()
		{
			SelectedTask.Items.Clear();
			foreach (UserTask u in UserTaskRetreival.GetAllTasks()) SelectedTask.Items.Add(u.Name);
		}

		private void ConfirmButton_Click(object sender, RoutedEventArgs e)
		{
			//Validate
			if(string.IsNullOrWhiteSpace(SlotTitle.Text))
			{
				MessageBox.Show("Please fill a title for the slot");
				return;
			}
			if(SlotDate.SelectedDate == null)
			{
				MessageBox.Show("Pleaes select a date");
				return;
			}

			//Save
			UserTask selectedTask = UserTaskRetreival.GetTaskByName((string)SelectedTask.SelectedItem);
			DateTime start = (DateTime)SlotDate.SelectedDate + StartTime.Time;
			DateTime end = (DateTime)SlotDate.SelectedDate + EndTime.Time;

			if (edit != null)
			{
				edit.Title = SlotTitle.Text;
				edit.StartTime = start.TimeOfDay;
				edit.EndTime = end.TimeOfDay;
				edit.StartDate = start.Date;
				edit.UserTask = selectedTask;
				edit.Repeat = (RepeatType)RepeatOptions.SelectedItem;
				edit.Update();
			}
			else
			{
				PlanSlot slot = new PlanSlot(Guid.NewGuid(), SlotTitle.Text, selectedTask.Id, start, start.TimeOfDay, end.TimeOfDay, (RepeatType)RepeatOptions.SelectedItem);
				slot.Update();
			}
			Close();
		}

		/// <summary>
		/// Called when the new task button is clicked. Opens a dialog for creating a task.
		/// </summary>
		private void NewTask_Click(object sender, RoutedEventArgs e)
		{
			new TaskEditDialog(LoadTasks).ShowDialog();
		}
	}
}
