﻿using System.Windows;
using TASKer.Controls;
using TASKer.Logic.Mail.Constraint;
using TASKer.Pages;

namespace TASKer.Dialogs
{
	/// <summary>
	/// Interaction logic for NewMailGroup.xaml
	/// 
	/// This dialog is used for editing a mail group.
	/// </summary>
	public partial class MailGroupEditDialog : RefreshingDialog
	{

		/// <summary>
		/// Creates a mail group edit dialog.
		/// </summary>
		/// <param name="refresh">Optional refresh method to be called when confirm is pressed.</param>
		public MailGroupEditDialog(RefreshTasksMethod refresh = null) : base(refresh)
		{
			InitializeComponent();
			Constraint.Constraint = new AndMailConstraint();
		}

		/// <summary>
		/// Called when confirm is clicked. Saves the MailGroup.
		/// </summary>
		private void Done_Click(object sender, RoutedEventArgs e)
		{
			//Validate the form
			if (string.IsNullOrEmpty(GroupName.Text))
			{
				MessageBox.Show("Please enter a name for the group.");
				return;
			}
			if(!Constraint.IsValid())
			{
				MessageBox.Show("Please fill all parameters of constraints.");
				return;
			}

			//Save
			((Application.Current.MainWindow as MainWindow).MainFrame.Content as MailViewPage).AddGroup(GroupName.Text, Constraint.Constraint);

			new DbMailConstraint(GroupName.Text, Constraint.Constraint).Update();

			Close();
		}
	}
} 
