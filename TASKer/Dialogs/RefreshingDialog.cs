﻿using System;
using System.Windows;

namespace TASKer.Dialogs
{

	//Delegate for refresh methods - they take nothing and return nothing.
	public delegate void RefreshTasksMethod();

	/// <summary>
	/// Window subclass for dialogs that have an optional refresh parameter that is a 
	/// method that is called when the dialog ends. The refresh is used to update UI
	/// in parent windows.
	/// </summary>
	public class RefreshingDialog : Window
	{
		
		//The refresh event
		private readonly RefreshTasksMethod refresh;

		/// <summary>
		/// Constructor for all refreshing dialogs.
		/// </summary>
		/// <param name="refresh">Optional refresh method</param>
		public RefreshingDialog(RefreshTasksMethod refresh = null)
		{
			this.refresh = refresh;
		}

		/// <summary>
		/// Called when the dialog is closed. This invokes the refresh if it exists.
		/// </summary>
		protected override void OnClosed(EventArgs e)
		{
			base.OnClosed(e);

			refresh?.Invoke();
		}

	}
}
