﻿using System;
using System.Linq;
using System.Windows;
using TASKer.Dialogs;
using TASKer.Logic;
using TASKer.Logic.APIs;

namespace TASKer
{
	/// <summary>
	/// Interaction logic for NewTask.xaml
	/// 
	/// A dialog for creating a new task.
	/// </summary>
	public partial class TaskEditDialog : RefreshingDialog
	{

		//A task to edit instead of creating a new one
		private readonly UserTask taskToEdit;

		/// <summary>
		/// Create a new text edit dialog.
		/// </summary>
		/// <param name="refresh">Optional method to call when the dialog is completed.</param>
		/// <param name="taskToEdit">Optional task to edit instead of creating a new one.</param>
		public TaskEditDialog(RefreshTasksMethod refresh = null, UserTask taskToEdit = null) : base(refresh)
		{
			InitializeComponent();

			this.taskToEdit = taskToEdit;

			LoadCategories();

			if (taskToEdit != null)
			{
				TaskTitle.Text = taskToEdit.Name;
				TaskDueDate.SelectedDate = taskToEdit.Time.Date;
				TaskDueTime.Hours = taskToEdit.Time.Hour;
				TaskDueTime.Minutes = taskToEdit.Time.Minute;
				TaskDuration.Hours = taskToEdit.Duration / 60;
				TaskDuration.Minutes = taskToEdit.Duration % 60;
				if (taskToEdit.Category != null)
					SelectedCategory.SelectedItem = taskToEdit.Category.Name;
			}

		}

		/// <summary>
		/// Loads all the categories to the category selection combobox.
		/// Called at start and when a new category is created using the new category button.
		/// </summary>
		private void LoadCategories()
		{
			SelectedCategory.Items.Clear();
			foreach (TaskCategory category in CategoryRetreival.GetAllCategories())
			{
				SelectedCategory.Items.Add(category.Name);
			}
		}

		/// <summary>
		/// Called when the confirm button is clicked. 
		/// Creates/saves the task.
		/// </summary>
		private void ConfirmButton_Click(object sender, RoutedEventArgs e)
		{
			//Validate form
			if (TaskDueDate.SelectedDate == null)
			{
				MessageBox.Show("Please select a date");
				return;
			}
			if (TaskTitle.Text == "Enter Title" || string.IsNullOrWhiteSpace(TaskTitle.Text))
			{
				MessageBox.Show("Please select a title");
				return;
			}
			if (SelectedCategory.SelectedItem == null)
			{
				MessageBox.Show("Please select a category");
				return;
			}

			//Save/create
			string name = TaskTitle.Text;
			DateTime date = (DateTime)TaskDueDate.SelectedDate;
			date = date.AddHours(TaskDueTime.Hours).AddMinutes(TaskDueTime.Minutes);

			int duration = TaskDuration.Hours * 60 + TaskDuration.Minutes;

			TaskCategory category = CategoryRetreival.GetCategoryByName((string)SelectedCategory.SelectedItem);

			if (taskToEdit == null)
			{
				new UserTask(name, date, duration, category.Id, 0).Update();
			}
			else
			{
				taskToEdit.Name = name;
				taskToEdit.Time = date;
				taskToEdit.Duration = duration;
				taskToEdit.Category = category;
				taskToEdit.Update();
			}

			Close();
		}

		/// <summary>
		/// Called when the new category button is clicked. Opens a dialog for creating
		/// a new category.
		/// </summary>
		private void NewCategory_Click(object sender, RoutedEventArgs e)
		{
			new CategoryEditDialog(LoadCategories).ShowDialog();
		}

	}
}
