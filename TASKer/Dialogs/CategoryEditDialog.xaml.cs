﻿using System.Windows;
using TASKer.Logic;

namespace TASKer.Dialogs
{
	/// <summary>
	/// Interaction logic for NewCategory.xaml
	/// </summary>
	public partial class CategoryEditDialog : RefreshingDialog
	{

		//The category this is editing.
		private TaskCategory category;

		/// <summary>
		/// Create a CategoryEdit window with the given refresh method and an optional task to edit.
		/// </summary>
		/// <param name="refresh">A method that will be called when confirm is pressed, to update UI.</param>
		/// <param name="edit">An optional Category to edit. If null (defualt), creates a new category.</param>
		public CategoryEditDialog(RefreshTasksMethod refresh = null, TaskCategory edit = null) : base(refresh)
		{
			InitializeComponent();

			category = edit;

			if (edit != null)
				CategoryName.Text = edit.Name;
		}

		/// <summary>
		/// Called when the user clicks submit. Saves the category.
		/// </summary>
		private void Submit_Click(object sender, RoutedEventArgs e)
		{
			//Validate the form
			if(string.IsNullOrWhiteSpace(CategoryName.Text))
			{
				MessageBox.Show("Please enter a category name");
				return;
			}

			//Save / create
			if (category != null)
			{
				category.Name = CategoryName.Text;
				category.Update();
			}
			else
				new TaskCategory(CategoryName.Text).Update();

			Close();
		}
	}
}
