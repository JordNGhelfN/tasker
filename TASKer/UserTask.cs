﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace TASKer.Logic
{
	public class UserTask
	{

		[Key]
		public Guid Id { get; set; }

		public string Name { get; set; }
		public DateTime Time { get; set; }

		public UserTask(string name, DateTime time)
		{
			this.Name = name;
			this.Time = time;
		}
	}
}
