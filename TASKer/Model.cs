﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic
{
	public class TASKerContext : DbContext
	{
		public DbSet<UserTask> UserTasks { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=TASKer.db");
	}

}
