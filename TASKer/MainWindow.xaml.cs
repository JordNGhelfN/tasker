﻿using System;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Navigation;
using TASKer.Logic.Database;
using TASKer.Logic.Database.SQLite;
using TASKer.Logic.Mail;
using TASKer.Pages;

namespace TASKer
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();

			//Create the database builder instance.
			ContextBuilder.Instance = new SQLiteContextBuilder();
			using (GmailClient client = new GmailClient())
				GmailUserInfo.Current.Address = client.GetMe();

			MainFrame.Navigate(new HomePage());

			BackButton.Visibility = Visibility.Hidden;
		}

		private void BackButton_Click(object sender, RoutedEventArgs e)
		{
			MainFrame.GoBack();
		}

		private void MainFrame_Navigated(object sender, NavigationEventArgs e)
		{
			if (!MainFrame.CanGoBack) BackButton.Visibility = Visibility.Hidden;
			else BackButton.Visibility = Visibility.Visible;

			if(MainFrame.Content is HomePage hp)
			{
				new Thread(hp.DrawEmails).Start();
				hp.mainMail.Children.Clear();
				hp.mainMail.Children.Add(new TextBlock() { Text = "Loading mail..." });
				hp.DrawTasks();
			}
		}

		private void TitleBar_MouseDown(object sender, MouseButtonEventArgs e)
		{
			if (e.ChangedButton == MouseButton.Left)
				if (e.ClickCount == 2)
				{
					AdjustWindowSize();
				}
				else
				{
					Application.Current.MainWindow.DragMove();
				}
		}

		/// <summary>
		/// CloseButton_Clicked
		/// </summary>
		private void CloseButton_Click(object sender, RoutedEventArgs e)
		{
			Environment.Exit(0);
		}

		/// <summary>
		/// MaximizedButton_Clicked
		/// </summary>
		private void MaximizeButton_Click(object sender, RoutedEventArgs e)
		{
			AdjustWindowSize();
		}

		/// <summary>
		/// Minimized Button_Clicked
		/// </summary>
		private void MinimizeButton_Click(object sender, RoutedEventArgs e)
		{
			WindowState = WindowState.Minimized;
		}

		/// <summary>
		/// Adjusts the WindowSize to correct parameters when Maximize button is clicked
		/// </summary>
		private void AdjustWindowSize()
		{
			if (this.WindowState == WindowState.Maximized)
			{
				WindowState = WindowState.Normal;
			}
			else
			{
				WindowState = WindowState.Maximized;
			}

		}
	}
}
