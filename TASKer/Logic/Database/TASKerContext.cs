﻿using Microsoft.EntityFrameworkCore;
using TASKer.Logic.Mail.Constraint;
using TASKer.Logic.Plan;

namespace TASKer.Logic.Database
{

	/// <summary>
	/// This object is a database connection, and stores DbSet objects for tables in the database.
	/// </summary>
	public abstract class TASKerContext : DbContext
	{
		public DbSet<UserTask> UserTasks { get; set; }
		public DbSet<TaskCategory> TaskCategories { get; set; }
		public DbSet<DbMailConstraint> Constraints { get; set; }
		public DbSet<PlanSlot> PlanSlots { get; set; }

	}
}
