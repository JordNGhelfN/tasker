﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.ComponentModel.DataAnnotations;
using TASKer.Logic.Mail.Constraint;
using TASKer.Logic.Plan;

namespace TASKer.Logic.Database.SQLite
{

	/// <summary>
	/// A TASKerContext connected to the local SQLite database.
	/// </summary>
	public class SQLiteTASKerContext : TASKerContext
	{
		protected override void OnConfiguring(DbContextOptionsBuilder options) => options.UseSqlite("Data Source=../../../Logic/Database/SQLite/TASKer.db");
	}

}
