﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TASKer.Logic.Database;

namespace TASKer.Logic
{

	/// <summary>
	/// A collection of tasks grouped by name.
	/// </summary>
	public class TaskCategory
	{

		//Id of the category.
		[Key]
		public Guid Id { get; set; }

		//Name of the category.
		public string Name { get; set; }

		/// <summary>
		/// Create an empty task category.
		/// </summary>
		/// <param name="name">Name of the category.</param>
		public TaskCategory(string name)
		{
			Name = name;
		}

		/// <summary>
		/// Delete this task category.
		/// </summary>
		public void Delete()
		{
			using var db = ContextBuilder.Instance.Build();
			db.Remove(this);
			db.SaveChanges();
		}

		/// <summary>
		/// Update this category.
		/// </summary>
		internal void Update()
		{
			using var db = ContextBuilder.Instance.Build();
			if (db.TaskCategories.Any(s => s.Id == Id))
				db.TaskCategories.Update(this);
			else
				db.TaskCategories.Add(this);
			db.SaveChanges();
		}
	}
}
