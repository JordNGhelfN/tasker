﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Plan
{
	/// <summary>
	/// Types of repeat options for slots
	/// </summary>
	public enum RepeatType
	{

		Never, Daily, Weekly, Monthly, Yearly,

	}
}
