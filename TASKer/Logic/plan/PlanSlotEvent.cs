﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Plan
{

	/// <summary>
	/// An occurence of a repeating slot in some date.
	/// </summary>
	public class PlanSlotEvent
	{

		//Date this event happens on.
		private DateTime date;
		//public property for date.
		public DateTime Date { get { return date; } set { date = value.Date; } }

		//Slot this event relates to.
		public PlanSlot Slot { get; set; }

		/// <summary>
		/// Create a plan slot event from a given slot on a given day
		/// </summary>
		/// <param name="slot">Slot that happens</param>
		/// <param name="date">Date it happens on</param>
		public PlanSlotEvent(PlanSlot slot, DateTime date)
		{
			Date = date.Date;
			Slot = slot;
		}

		//Get the real start of the event on the given date
		public DateTime Start { get { return Date + Slot.StartTime; } }
		//Get the real end of the event on the given date
		public DateTime End { get { return Date + Slot.EndTime; } }

	}
}
