﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TASKer.Logic.Database;

namespace TASKer.Logic.Plan
{

	/// <summary>
	/// Static methods for creating a weekly task plan
	/// </summary>
	public static class PlanGenerator
	{

		/// <summary>
		/// Helper method to get the current assigned task in the current weekly plan.
		/// </summary>
		/// <returns>Task that is assigned to now, or null if no task is assigned.</returns>
		public static UserTask GetCurrentTask()
		{
			using var db = ContextBuilder.Instance.Build();
			foreach(PlanSlot slot in db.PlanSlots)
			{
				if (slot.UserTask == null) continue;
				if (!slot.RepeatOnDate(DateTime.Today)) continue;
				if (slot.StartTime < DateTime.Now.TimeOfDay && DateTime.Now.TimeOfDay < slot.EndTime) return slot.UserTask;
			}
			return null;
		}

		/// <summary>
		/// Generate a new plan between two times
		/// </summary>
		/// <param name="first">Time to generate slots after</param>
		/// <param name="last">Time to generate slots before</param>
		public static void Generate(DateTime first, DateTime last)
		{
			//Delete current plan
			using (var db = ContextBuilder.Instance.Build()) db.RemoveRange(db.PlanSlots.Where(s => !s.Locked));
			//Get slots to assign
			(List<PlanSlotEvent> locked, List<UserTask> unlocked) = GetSlots(first, last);
			//Assign the slots
			AssignSlots(locked, unlocked, first, last);
		}

		/// <summary>
		/// Generate a new plan between two dates, while postponing a task to not be assigned to now.
		/// </summary>
		/// <param name="first">Time to generate slots after</param>
		/// <param name="last">Time to generate slots before</param>
		/// <param name="postpone">Task to not assign to now</param>
		public static void GenerateAndPostpone(DateTime first, DateTime last, UserTask postpone = null)
		{
			if (postpone == null) postpone = GetCurrentTask();
			using (var db = ContextBuilder.Instance.Build()) db.RemoveRange(db.PlanSlots.Where(s => !s.Locked));
			(List<PlanSlotEvent> locked, List<UserTask> unlocked) = GetSlots(first, last);
			if(unlocked[0] == postpone)
			{
				unlocked[0] = unlocked[1];
				unlocked[1] = postpone;
			}
			AssignSlots(locked, unlocked, first, last);
		}

		/// <summary>
		/// Get slots to assign for tasks between two dates, as well as locked slots in the range.
		/// </summary>
		/// <param name="first">Dates to start getting tasks from</param>
		/// <param name="last">Dates to end getting tasks from</param>
		/// <returns>locked - locked slots in the time range. Unlocked - slots for tasks that are due in the time range</returns>
		private static (List<PlanSlotEvent> locked, List<UserTask> unlocked) GetSlots(DateTime first, DateTime last)
		{
			List<PlanSlotEvent> locked = new List<PlanSlotEvent>();
			List<UserTask> unlocked = new List<UserTask>();

			using (var db = ContextBuilder.Instance.Build())
			{

				//Add locked slots
				foreach (PlanSlot slot in db.PlanSlots)
				{
					if (slot.Locked) locked.AddRange(slot.GetAllRepeatsBetween(first, last).Where(s => s.End > first && s.Start < last));
					else
					{
						db.PlanSlots.Remove(slot);
					}
				}
				db.SaveChanges();

				List<UserTask> tasks = db.UserTasks.Where(t => (t.DoneInt == 0 && t.Time > first)).ToList();
				foreach (UserTask task in tasks)
				{
					bool alreadyIn = false;
					foreach (PlanSlotEvent s in locked) 
						if (s.Slot.UserTask == task) 
							alreadyIn = true;
					if (alreadyIn) continue;

					unlocked.Add(task);
				}
			}
			locked = locked.OrderBy(s => s.Start).ToList();
			unlocked = unlocked.OrderBy(t => t.Time).ThenByDescending(t => t.Duration).ToList();
			return (locked, unlocked);
		}

		/// <summary>
		/// Assign unlocked slots so that they do not overlap the locked ones.
		/// </summary>
		/// <param name="locked">Locked slots</param>
		/// <param name="unlocked">Unlocked slots (for tasks)</param>
		/// <param name="first">Timerange to start assigning from</param>
		/// <param name="last">Timerange to end assignments</param>
		private static void AssignSlots(List<PlanSlotEvent> locked, List<UserTask> unlocked, DateTime first, DateTime last)
		{
			DateTime current = first;

			List<PlanSlot> slots = new List<PlanSlot>();
			if (unlocked.Count == 0) return;
			UserTask currentTask = unlocked[0];
			unlocked.RemoveAt(0);
			double currentTaskRemainingTime = currentTask.Duration;

			while (current < last && locked.Count > 0)
			{
				PlanSlotEvent nextLocked = locked[0];
				locked.RemoveAt(0);

				TimeSpan gap = nextLocked.Start - current;
				if ((current + gap).Date > current.Date) gap = new TimeSpan(23, 59, 0) - current.TimeOfDay;
				while (gap > TimeSpan.FromMinutes(30))
				{
					if (gap.TotalMinutes > currentTaskRemainingTime)
					{
						TimeSpan timeRemaining = TimeSpan.FromMinutes(currentTaskRemainingTime);
						PlanSlot slot = new PlanSlot(
							Guid.NewGuid(),
							currentTask.Name,
							currentTask.Id,
							current.Date,
							current.TimeOfDay,
							current.TimeOfDay + timeRemaining,
							RepeatType.Never,
							false
							);
						slots.Add(slot);
						currentTaskRemainingTime = 0;
						if (unlocked.Count == 0) break;

						currentTask = unlocked[0];
						unlocked.RemoveAt(0);
						currentTaskRemainingTime = currentTask.Duration;

						current += timeRemaining;
						if(current.TimeOfDay > new TimeSpan(23, 59, 0))
						{
							current = current.AddDays(1).Date;
						}
						gap -= timeRemaining;
					}
					else
					{
						PlanSlot slot = new PlanSlot(
							Guid.NewGuid(),
							currentTask.Name,
							currentTask.Id,
							current.Date,
							current.TimeOfDay,
							current.TimeOfDay + gap,
							RepeatType.Never,
							false
							);
						slots.Add(slot);
						currentTaskRemainingTime -= gap.TotalMinutes;
						gap -= gap;
					}
				}

				if (unlocked.Count == 0 && currentTaskRemainingTime <= 0) break;
				current = nextLocked.End;
				if (current.TimeOfDay >= new TimeSpan(23, 59, 0))
				{
					current = current.AddDays(1).Date;
				}
			}

			using var db = ContextBuilder.Instance.Build();
			db.PlanSlots.AddRange(slots);
			db.SaveChanges();
		}

	}
}
