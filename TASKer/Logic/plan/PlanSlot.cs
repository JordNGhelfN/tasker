﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using TASKer.Logic.Database;
using TASKer.Util;

namespace TASKer.Logic.Plan
{
	/// <summary>
	/// An assigned slot for doing something at a specific time in a week plan
	/// </summary>
	public class PlanSlot
	{
		//Database id
		[Key]
		public Guid Id { get; set; }

		//Title of the slot
		public string Title { get; set; }

		//Task the slot is for, if there is one (Guid.Empty otherwise)
		[ForeignKey("UserTask")]
		public Guid UserTaskId { get; set; }

		//First date this slot starts repeating in (or the date this slot appears on if set to no repeat).
		public DateTime StartDate { get; set; }
		//Start time of day of the slot
		public TimeSpan StartTime { get; set; }
		//End time of day of the slot
		public TimeSpan EndTime { get; set; }

		//Repeat type for this slot.
		public RepeatType Repeat { get; set; }

		//Whether this slot is locked (doesn't get unassigned when regenerating plan).
		public bool Locked { get; set; }

		//Getter for the user task for this slot.
		[NotMapped]
		public UserTask UserTask
		{
			get
			{
				using var db = ContextBuilder.Instance.Build();
				return db.UserTasks.Find(UserTaskId);
			}
			set
			{
				if (value == null) UserTaskId = Guid.Empty;
				else UserTaskId = value.Id;
			}
		}

		/// <summary>
		/// Empty default constructor for Entity Framework.
		/// </summary>
		public PlanSlot()
		{

		}

		/// <summary>
		/// Create a plan slot with all the data
		/// </summary>
		/// <param name="id">Id for the slot</param>
		/// <param name="title">Title for the slot</param>
		/// <param name="userTaskId">Id of the task for the slot</param>
		/// <param name="date">Date this slot starts repeating on</param>
		/// <param name="start">Time of day the slot starts in</param>
		/// <param name="end">Time of day the slot ends in</param>
		/// <param name="repeat">Repeat options for this slot</param>
		/// <param name="locked">Whether the slot is locked</param>
		public PlanSlot(Guid id, string title, Guid userTaskId, DateTime date, TimeSpan start, TimeSpan end, RepeatType repeat, bool locked=true)
		{
			Id = id;
			Title = title;
			UserTaskId = userTaskId;
			StartDate = date;
			StartTime = start;
			EndTime = end;
			Repeat = repeat;
			Locked = locked;
		}

		/// <summary>
		/// Check if this slot repeats on a given date.
		/// </summary>
		/// <param name="date">The date to check repeat on</param>
		/// <returns>True if this slot repeats on the date, false otherwise.</returns>
		public bool RepeatOnDate(DateTime date)
		{
			date = date.Date;

			//First check if date is identical.
			if (date == StartDate) return true;

			//Check repeats. Assure that the first time has already happened first.
			if (date < StartDate) return false;

			//Check repeat types.
			if (Repeat == RepeatType.Daily) return true;
			if (Repeat == RepeatType.Weekly) return date.DayOfWeek == StartDate.DayOfWeek;
			if (Repeat == RepeatType.Monthly) return date.Day == StartDate.Day;
			if (Repeat == RepeatType.Yearly) return date.DayOfYear == StartDate.DayOfYear;

			//No repeat, return false.
			return false;
		}

		/// <summary>
		/// Get all repeats of this slot in a given time range
		/// </summary>
		/// <param name="first">Time range start</param>
		/// <param name="last">Time range end</param>
		/// <returns>List of PlanSlotEvents for the repeated event</returns>
		public List<PlanSlotEvent> GetAllRepeatsBetween(DateTime first, DateTime last)
		{
			List<PlanSlotEvent> repeats = new List<PlanSlotEvent>();
			foreach (DateTime date in DatesUtils.GetDatesBetween(first, last))
			{
				DateTime start = date + StartTime;
				DateTime end = date + EndTime;
				if (RepeatOnDate(date))
				{
					PlanSlotEvent n = new PlanSlotEvent(this, date);
					repeats.Add(n);
				}
			}
			return repeats;
		}

		/// <summary>
		/// Update this slot in the database.
		/// </summary>
		public void Update()
		{
			using var db = ContextBuilder.Instance.Build();
			if (db.PlanSlots.Any(s => s.Id == Id))
				db.PlanSlots.Update(this);
			else
				db.PlanSlots.Add(this);
			db.SaveChanges();
		}
	}
}
