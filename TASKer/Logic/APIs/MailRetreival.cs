﻿using System;
using System.Collections.Generic;
using System.Text;
using TASKer.Logic.Database;
using TASKer.Logic.Mail;
using TASKer.Logic.Mail.Constraint;

namespace TASKer.Logic.APIs
{

	/// <summary>
	/// Static methods for retreiving mail related database objects, like mail groups and messages
	/// </summary>
	public static class MailRetreival
	{

		/// <summary>
		/// Get messages from a given page of a given size
		/// </summary>
		/// <param name="count">messages per page</param>
		/// <param name="page">page index</param>
		/// <returns>list of count messages in the given page</returns>
		public static List<MailMessage> GetMessages(int count, int page)
		{
			using var client = new GmailClient();
			return client.GetMessages(count, page);
		}

		/// <summary>
		/// Get all mail groups in the database for a list of messages
		/// </summary>
		/// <param name="messages">list of all loaded messages</param>
		/// <returns>List of mail groups with those messages</returns>
		public static List<MailGroup> GetAllMailGroups(List<MailMessage> messages)
		{
			List<MailGroup> groups = new List<MailGroup>();
			MailGroup main = new MailGroup("main", messages);
			groups.Add(main);

			using var db = ContextBuilder.Instance.Build();
			foreach (DbMailConstraint dbMailConstraint in db.Constraints)
			{
				MailConstraint constraint = MailConstraint.DecodeConstraint(dbMailConstraint.Constraint);
				groups.Add(new MailGroup(dbMailConstraint.Name, messages, constraint));
			}

			return groups;
		}

		/// <summary>
		/// Get a mail group by name from the database
		/// </summary>
		/// <param name="name">name to search for</param>
		/// <param name="messages">messages to create the group from</param>
		/// <returns>A mail group with the given name</returns>
		public static MailGroup GetGroupByName(string name, List<MailMessage> messages)
		{
			if (name == "main") return new MailGroup("main", messages);

			using var db = ContextBuilder.Instance.Build();
			foreach (DbMailConstraint dbMailConstraint in db.Constraints)
			{
				if (dbMailConstraint.Name == name) return new MailGroup(name, messages, MailConstraint.DecodeConstraint(dbMailConstraint.Constraint));
			}
			throw new KeyNotFoundException("Group with name not found");
		}
	}
}
