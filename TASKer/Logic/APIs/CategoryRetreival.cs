﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TASKer.Logic.Database;

namespace TASKer.Logic.APIs
{

	/// <summary>
	/// Provides an interface for retreival of task categories from the database.
	/// </summary>
	public static class CategoryRetreival
	{
		/// <summary>
		/// Retreive a category by name from the database.
		/// </summary>
		/// <param name="name">The name of the category to retreive</param>
		/// <returns>The found category or null if none found</returns>
		public static TaskCategory GetCategoryByName(string name)
		{
			using var db = ContextBuilder.Instance.Build();
			return db.TaskCategories.Where(c => c.Name == name).FirstOrDefault();
		}

		/// <summary>
		/// Get all categories in the db.
		/// </summary>
		/// <returns>All categories in the db.</returns>
		public static List<TaskCategory> GetAllCategories()
		{
			using var db = ContextBuilder.Instance.Build();
			return db.TaskCategories.ToList();
		}

	}
}
