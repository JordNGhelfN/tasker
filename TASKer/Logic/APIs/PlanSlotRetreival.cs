﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TASKer.Logic.Database;
using TASKer.Logic.Plan;

namespace TASKer.Logic.APIs
{
	/// <summary>
	/// Static methods for retreiving PlanSlots from the database for common use cases
	/// </summary>
	public static class PlanSlotRetreival
	{

		/// <summary>
		/// Get all plan slots on the date of a given time, starting from the given time
		/// </summary>
		/// <param name="start">Time to find slots on the same date, ending after the time.</param>
		/// <returns>List of slots starting on the same date as specified and end after the time specified.</returns>
		public static List<PlanSlot> GetPlanSlotsOnDayAfterTime(DateTime start)
		{
			using var db = ContextBuilder.Instance.Build();
			List<PlanSlot> slots = new List<PlanSlot>();
			foreach (PlanSlot s in db.PlanSlots)
			{
				if (!s.RepeatOnDate(DateTime.Today)) continue;
				if (s.EndTime < DateTime.Now.TimeOfDay) continue;
				slots.Add(s);
			}
			return slots;
		}

		/// <summary>
		/// Get all plan slots in the database.
		/// </summary>
		/// <returns>A list of all plan slots.</returns>
		public static List<PlanSlot> GetAllPlanSlots()
		{
			using var db = ContextBuilder.Instance.Build();
			return db.PlanSlots.ToList();
		}

	}
}
