﻿using Google.Apis.Gmail.v1.Data;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace TASKer.Logic.Mail
{

	/// <summary>
	/// A collection of messages that are in the same conversation (thread).
	/// </summary>
	public class MailConversation
	{

		//GMAIL API Thread Id
		public string ConversationId { get; set; }

		//List of messages in the conversation
		public List<MailMessage> Messages { get; }

		/// <summary>
		/// Create a mail conversation
		/// </summary>
		/// <param name="conversationId">GMAIL API thread id</param>
		public MailConversation(string conversationId)
		{
			ConversationId = conversationId;
			Messages = new List<MailMessage>();

			using GmailClient client = new GmailClient();
			foreach (Message m in client.GetRawConversation(conversationId))
			{
				Messages.Add(new MailMessage(m, this));
			}
		}
	}
}
