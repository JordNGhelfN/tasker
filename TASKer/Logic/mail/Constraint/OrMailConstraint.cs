﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Mail.Constraint
{

	/// <summary>
	/// A logical or mail constraint. Satisfied if one of the children is satisfied.
	/// </summary>
	public class OrMailConstraint : MailConstraint
	{

		//Supports infinate children.
		public override int AllowedChildren => int.MaxValue;

		/// <summary>
		/// Encode the constraint and the children.
		/// </summary>
		/// <returns>string representing the constraint.</returns>
		public override string Encode()
		{
			string s = "or(";
			foreach (MailConstraint constraint in Children) s += constraint.Encode() + ",";
			s += ")";
			return s;
		}


		/// <summary>
		/// Check if a message is satisfied by this constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the message is matched, false otherwise.</returns>
		public override bool IsMatch(MailMessage message)
		{
			foreach (MailConstraint m in Children) if (m.IsMatch(message)) return true;
			return false;
		}
	}
}
