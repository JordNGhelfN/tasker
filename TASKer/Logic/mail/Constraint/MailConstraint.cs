﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Mail.Constraint
{


	/// <summary>
	/// This class represents a constraint on an email message.
	/// A constraint is a set of rules that specifiy which email messages should go in
	/// a group.
	/// </summary>
	public abstract class MailConstraint
	{

		//The children of the constraint (sub-rules).
		public List<MailConstraint> Children { get; }

		/// <summary>
		/// General constructor for constraints that takes a name.
		/// </summary>
		/// <param name="name"></param>
		public MailConstraint()
		{
			Children = new List<MailConstraint>();
		}

		/// <summary>
		/// Abstract method to test if a message satisfies the constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the constraint is satisfied and false otherwise.</returns>
		public abstract bool IsMatch(MailMessage message);

		/// <summary>
		/// Abstract method to return the amount of allowed children for a constraint.
		/// </summary>
		public abstract int AllowedChildren { get; }

		/// <summary>
		/// Abstract method to convert a constraint to string representation for storing in
		/// the database.
		/// </summary>
		/// <returns></returns>
		public abstract string Encode();

		/// <summary>
		/// Static method for decoding an encoded constraint.
		/// </summary>
		/// <param name="code">The encoded constraint in string form.</param>
		/// <returns>A constraint object from the string.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "Those are strings encoded with no regards to culture and never interact with users")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Globalization", "CA1307:Specify StringComparison", Justification = "Those are strings encoded with no regards to culture and never interact with users")]
		public static MailConstraint DecodeConstraint(string code)
		{
			string inner = code.Substring(code.IndexOf('('));
			inner = inner.Remove(inner.Length - 1);
			inner = inner.Remove(0, 1);

			string[] children = inner.Split(',', StringSplitOptions.RemoveEmptyEntries);

			if (code.StartsWith("and"))
			{
				AndMailConstraint constraint = new AndMailConstraint();
				foreach (string child in children) constraint.Children.Add(DecodeConstraint(child));
				return constraint;
			}
			if (code.StartsWith("or"))
			{
				OrMailConstraint constraint = new OrMailConstraint();
				foreach (string child in children) constraint.Children.Add(DecodeConstraint(child));
				return constraint;
			}
			if (code.StartsWith("not"))
			{
				NotMailConstraint constraint = new NotMailConstraint();
				foreach (string child in children) constraint.Children.Add(DecodeConstraint(child));
				return constraint;
			}
			if (code.StartsWith("sender"))
			{
				SenderMailConstraint constraint = new SenderMailConstraint(children[0]);
				return constraint;
			}
			if (code.StartsWith("subject"))
			{
				SubjectMailConstraintType type = children[0] == "contains" ? SubjectMailConstraintType.contains : SubjectMailConstraintType.equals;
				SubjectMailConstraint constraint = new SubjectMailConstraint(type, children[1]);
				return constraint;
			}

			throw new Exception("Invalid constraint code");
		}

	}
}
