﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using TASKer.Logic.Database;

namespace TASKer.Logic.Mail.Constraint
{

	/// <summary>
	/// A wrapper around the mail constraint that has a name and can be stored in the db.
	/// It saves the constraint in its string representation.
	/// </summary>
	public class DbMailConstraint
	{

		//The name of the constraint, for db purposes.
		[Key]
		public string Name { get; set; }
		
		/// <summary>
		/// The constraint string to save.
		/// </summary>
		public string Constraint { get; set; }

		/// <summary>
		/// Create a db constraint from a string representation.
		/// </summary>
		/// <param name="name">The name of the constraint</param>
		/// <param name="constraint">the constraint in string representation (use Encode, or use the constructor that takes a Constraint parameter)</param>
		public DbMailConstraint(string name, string constraint)
		{
			Name = name;
			Constraint = constraint;
		}

		/// <summary>
		/// Create a db constraint from a name and a raw constraint.
		/// </summary>
		/// <param name="name">The name for the constraint</param>
		/// <param name="constraint">The constraint to be saved in raw form.</param>
		public DbMailConstraint(string name, MailConstraint constraint)
		{
			Name = name;
			Constraint = constraint.Encode();
		}

		/// <summary>
		/// Save the constraint to the db.
		/// </summary>
		internal void Update()
		{
			using var db = ContextBuilder.Instance.Build();
			if (db.Constraints.Any(c => c.Name == Name))
				db.Constraints.Update(this);
			else
				db.Constraints.Add(this);
			db.SaveChanges();
		}

		internal void Delete()
		{
			using var db = ContextBuilder.Instance.Build();
			db.Remove(this);
			db.SaveChanges();
		}
	}
}
