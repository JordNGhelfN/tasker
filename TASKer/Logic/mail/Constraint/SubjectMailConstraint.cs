﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Mail.Constraint
{
	/// <summary>
	/// Enum for types of subject constraints - either the subjects contains the text or is identical.
	/// </summary>
	public enum SubjectMailConstraintType
	{
		equals, contains,
	}

	/// <summary>
	/// A constraint that checks the subject of the mail.
	/// </summary>
	public class SubjectMailConstraint : MailConstraint
	{

		//Does not support children.
		public override int AllowedChildren => 0;

		//The type of the constraint (contain or equal exactly).
		public SubjectMailConstraintType Type { get; set; }
		
		//The string parameter - what to look for in the subject.
		public string Param { get; set; }

		/// <summary>
		/// Create a subject mail constraint with the type and param.
		/// </summary>
		/// <param name="type">The subject constraint type</param>
		/// <param name="param">The parameter</param>
		public SubjectMailConstraint(SubjectMailConstraintType type, string param)
		{
			Type = type;
			Param = param;
		}

		/// <summary>
		/// Encode the constraint and the children.
		/// </summary>
		/// <returns>string representing the constraint.</returns>
		public override string Encode()
		{
			string s = "subject(";
			if (Type == SubjectMailConstraintType.contains) s += "contains,";
			if (Type == SubjectMailConstraintType.equals) s += "equals,";
			s += Param + ")";
			return s;
		}

		/// <summary>
		/// Check if a message is satisfied by this constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the message is matched, false otherwise.</returns>
		public override bool IsMatch(MailMessage message)
		{
			if (Type == SubjectMailConstraintType.contains)
				return message.Subject.Contains(Param, StringComparison.OrdinalIgnoreCase);
			if (Type == SubjectMailConstraintType.equals)
				return message.Subject == Param;

			return false;
		}
	}
}
