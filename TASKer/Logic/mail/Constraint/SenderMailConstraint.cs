﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace TASKer.Logic.Mail.Constraint
{
	/// <summary>
	/// A mail constraint that checks who the sender is.
	/// </summary>
	public class SenderMailConstraint : MailConstraint
	{

		//Does not support children.
		public override int AllowedChildren => 0;

		//Property for the param field.
		public string Param { get; set; }

		/// <summary>
		/// Create a sender mail constraint
		/// </summary>
		/// <param name="param">The sender parameter</param>
		public SenderMailConstraint(string param)
		{
			Param = param;
		}

		/// <summary>
		/// Encode the constraint and the children.
		/// </summary>
		/// <returns>string representing the constraint.</returns>
		public override string Encode()
		{
			string s = "sender(" + Param + ")";
			return s;
		}

		/// <summary>
		/// Check if a message is satisfied by this constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the message is matched, false otherwise.</returns>
		public override bool IsMatch(MailMessage message)
		{
			return message.Sender.ToLower().Contains(Param.ToLower());
		}
	}
}
