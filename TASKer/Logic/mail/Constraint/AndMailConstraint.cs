﻿namespace TASKer.Logic.Mail.Constraint
{
	/// <summary>
	/// A logical and mail constraint. It is satisfied if all its children are satisfied.
	/// </summary>
	public class AndMailConstraint : MailConstraint
	{
		//Supports infinate children.
		public override int AllowedChildren => int.MaxValue;

		/// <summary>
		/// Encode the constraint and the children.
		/// </summary>
		/// <returns>string representing the constraint.</returns>
		public override string Encode()
		{
			string s = "and(";
			foreach (MailConstraint constraint in Children) s += constraint.Encode() + ",";
			s += ")";
			return s;
		}

		/// <summary>
		/// Check if a message is satisfied by this constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the message is matched, false otherwise.</returns>
		public override bool IsMatch(MailMessage message)
		{
			foreach (MailConstraint c in Children) if (!c.IsMatch(message)) return false;
			return true;
		}
	}
}
