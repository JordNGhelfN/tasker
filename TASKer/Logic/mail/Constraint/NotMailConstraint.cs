﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TASKer.Logic.Mail.Constraint
{

	/// <summary>
	/// A logical not mail constraint. It matches when its single child does not.
	/// </summary>
	public class NotMailConstraint : MailConstraint
	{

		//Supports a single child.
		public override int AllowedChildren => 1;

		/// <summary>
		/// Encode the constraint and the children.
		/// </summary>
		/// <returns>string representing the constraint.</returns>
		public override string Encode()
		{
			string s = "not(";
			foreach (MailConstraint constraint in Children) s += constraint.Encode() + ",";
			s += ")";
			return s;
		}


		/// <summary>
		/// Check if a message is satisfied by this constraint.
		/// </summary>
		/// <param name="message">The message to check</param>
		/// <returns>True if the message is matched, false otherwise.</returns>
		public override bool IsMatch(MailMessage message)
		{
			return !Children[0].IsMatch(message);
		}
	}
}
