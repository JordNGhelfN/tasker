﻿using Google;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Gmail.v1;
using Google.Apis.Gmail.v1.Data;
using Google.Apis.Services;
using Google.Apis.Util.Store;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;

namespace TASKer.Logic.Mail
{
	/// <summary>
	/// A connection to the gmail API.
	/// </summary>
	public class GmailClient : IDisposable
	{
		//Permission scopes for the connection
		private readonly string[] Scopes = { GmailService.Scope.GmailSend, GmailService.Scope.GmailReadonly, GmailService.Scope.GmailModify };
		
		//Credentials for the connection
		private UserCredential credential;

		//The service object (from google API).
		private GmailService service;

		/// <summary>
		/// Create a new connection.
		/// </summary>
		public GmailClient()
		{
			//Create credential file if doesn't exist.
			using (var stream = new FileStream("credentials.json", FileMode.Open, FileAccess.Read))
			{
				string credPath = "google_token_7_9_20.json";
				credential = GoogleWebAuthorizationBroker.AuthorizeAsync(
					GoogleClientSecrets.Load(stream).Secrets,
					Scopes,
					"user",
					CancellationToken.None,
					new FileDataStore(credPath, true)).Result;
				Console.WriteLine("Credential file saved to: " + credPath);
			}

			//Create the service object
			service = new GmailService(new BaseClientService.Initializer()
			{
				HttpClientInitializer = credential,
				ApplicationName = "TASKer",
			});


		}

		/// <summary>
		/// Get the email address of the logged in user
		/// </summary>
		/// <returns></returns>
		public string GetMe()
		{
			return service.Users.GetProfile("me").Execute().EmailAddress;
		}

		/// <summary>
		/// Get a list of messages in a conversation. Messages come in the GMAIL API format.
		/// </summary>
		/// <param name="threadId">The thread id of the conversation</param>
		/// <returns>A list of all messages in the conversation in GMAIL API format</returns>
		public List<Message> GetRawConversation(string threadId)
		{
			Google.Apis.Gmail.v1.Data.Thread thread = service.Users.Threads.Get("me", threadId).Execute();
			return (List<Message>)thread.Messages;
		}

		/// <summary>
		/// Get messages in a given offset of a given count.
		/// </summary>
		/// <param name="count">Amount of messages per page</param>
		/// <param name="page">Page number to get</param>
		/// <returns>List of the count messages starting from the page*count message from the end. Messages are returned in TASKer format.</returns>
		public List<MailMessage> GetMessages(int count, int page)
		{
			//Get ids
			List<Google.Apis.Gmail.v1.Data.Thread> resultThread = new List<Google.Apis.Gmail.v1.Data.Thread>();
			UsersResource.ThreadsResource.ListRequest requestThread = service.Users.Threads.List("me");
			List<string> foundIds = new List<string>();

			int found = 0;

			//Get ids for the given page
			do
			{
				ListThreadsResponse responseThread = requestThread.Execute();
				resultThread.AddRange(responseThread.Threads);

				foreach (var item in responseThread.Threads)
				{
					found++;
					if (found > count * (page + 1))
						break;
					if (found >= page * count)
						foundIds.Add(item.Id);
				}

				requestThread.PageToken = responseThread.NextPageToken;

			} while (!String.IsNullOrEmpty(requestThread.PageToken) && found < count * (page + 1));

			//Get the mails
			List<MailMessage> mails = new List<MailMessage>();
			foreach (string id in foundIds)
			{
				try
				{
					mails.Add(new MailMessage(service.Users.Messages.Get("me", id).Execute()));
				}
				catch (GoogleApiException)
				{
					continue;
				}
			}
			return mails;
		}

		/// <summary>
		/// Send an email message to an address. 
		/// </summary>
		/// <param name="email">Message to send</param>
		/// <param name="to">Email address to send to</param>
		/// <param name="ReplyToId">Message id to reply to, default to null which means new conversation.</param>
		public void SendEmail(MailMessage email, string to, string ReplyToId = null)
		{
			var mailMessage = new System.Net.Mail.MailMessage();
			mailMessage.To.Add(to);
			mailMessage.Subject = email.Subject;
			mailMessage.Body = email.Body;

			var mimeMessage = MimeKit.MimeMessage.CreateFromMailMessage(mailMessage);
			if (ReplyToId != null)
				mimeMessage.InReplyTo = ReplyToId;

			var gmailMessage = new Google.Apis.Gmail.v1.Data.Message
			{
				Raw = Encode(mimeMessage.ToString())
			};

			Google.Apis.Gmail.v1.UsersResource.MessagesResource.SendRequest request = service.Users.Messages.Send(gmailMessage, "me");

			request.Execute();

			mailMessage.Dispose();
		}

		/// <summary>
		/// Convert message parts from GMAIL format to MimeKit format for parsing.
		/// </summary>
		/// <param name="text">Message string to parse</param>
		/// <returns>Message string with characters replaced for MimeKit format.</returns>
		private static string Encode(string text)
		{
			byte[] bytes = System.Text.Encoding.UTF8.GetBytes(text);

			return System.Convert.ToBase64String(bytes)
				.Replace('+', '-')
				.Replace('/', '_')
				.Replace("=", "");
		}

		/// <summary>
		/// Dispose of this client.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
			GC.SuppressFinalize(this);
		}
		/// <summary>
		/// Dispose of this client
		/// </summary>
		/// <param name="disposing"></param>
		protected virtual void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (service != null)
				{
					service.Dispose();
					service = null;
				}
			}
		}

		/// <summary>
		/// Move an email message to the trash.
		/// </summary>
		/// <param name="msg">Message to move</param>
		public void DeleteEmail(MailMessage msg)
		{
			if(msg == null)
			{
				throw new ArgumentNullException("msg", "Message to delete cannot be null");
			}
			service.Users.Messages.Trash("me", msg.Id).Execute();
		}
	}
}
