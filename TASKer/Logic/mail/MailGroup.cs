﻿using System;
using System.Collections.Generic;
using System.Text;
using TASKer.Logic.Mail.Constraint;

namespace TASKer.Logic.Mail
{

    /// <summary>
    /// A group of emails packed together by a MailConstrant.
    /// </summary>
    public class MailGroup
    {

        //The name of the group
        public string Name { get; set; }

        //List of messages in the group
        public List<MailMessage> Messages { get; set; }

        //The constraint that creates the group
        public MailConstraint Constraint { get; set; }


        /// <summary>
        /// Create a mail group with a name, a list of messages to get from, and a constraint to choose with.
        /// </summary>
        /// <param name="name">The name of the group</param>
        /// <param name="messages">Messages to get matching messages from</param>
        /// <param name="constraint">Constraint for choosing the messages.</param>
        public MailGroup(string name, List<MailMessage> messages, MailConstraint constraint = null)
        {
            Name = name;
            if(messages == null)
            {
                throw new ArgumentNullException(nameof(messages));
            }
            if (constraint == null)
                Messages = messages;
            else
                Messages = messages.FindAll(m => constraint.IsMatch(m));
            Constraint = constraint;
        }

    }
}
