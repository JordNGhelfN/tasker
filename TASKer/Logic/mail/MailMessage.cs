﻿using Google.Apis.Gmail.v1.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TASKer.Logic.Database;

namespace TASKer.Logic.Mail
{

	/// <summary>
	/// A mail message with a subject, sender and time, suitable for operations and for saving in the database.
	/// </summary>
	public class MailMessage
	{

		//The raw GoogleAPI message.
		private Message raw;
		
		//Id of the message
		public string Id { get; set; }

		//Subject of the message
		public string Subject { get; set; }

		//Sender address
		public string Sender { get; set; }

		//Sent time
		public DateTime Time { get; set; }

		//Message body string
		public string Body { get; set; }

		//Conversation this message is part of.
		public MailConversation Conversation { get; set; }

		/// <summary>
		/// Create a mail message from data
		/// </summary>
		/// <param name="subject">Mail message subject</param>
		/// <param name="sender">Email address that sent the mail</param>
		/// <param name="time">Time the mail was sent on</param>
		/// <param name="body">Message body</param>
		public MailMessage(string subject, string sender, DateTime time, string body)
		{
			Subject = subject;
			Sender = sender;
			Time = time;
			Body = body;
		}

		/// <summary>
		/// Create a message from a raw GoogleAPI message.
		/// At this point the body of the message will not be completely parsed, until Load() is 
		/// called to save time.
		/// </summary>
		/// <param name="raw">Message in GoogleAPI format</param>
		/// <param name="conversation">Conversation for this mail. If null (or default), a new conversation will be created.</param>
		public MailMessage(Message raw, MailConversation conversation = null)
		{
			this.raw = raw;
			if (conversation == null)
				Conversation = new MailConversation(raw?.ThreadId);

			Time = DateTime.Now;

			Id = raw.Id;

			foreach (var header in raw.Payload.Headers)
			{
				if (header.Name == "Subject") Subject = header.Value;
				if (header.Name == "From") Sender = header.Value;
				//if (header.Name == "Date") Time = header.Value;
			}
			Body = raw.Snippet;
		}


		//This region contains methods for parsing of raw GmailAPI messages.
		#region PARSE RAW MESSAGES

		/// <summary>
		/// Load the body of the message completely.
		/// </summary>
		public void Load()
		{	
			if (raw.Payload.Parts == null && raw.Payload.Body != null)
			{
				Body = DecodeBase64String(raw.Payload.Body.Data);
			}
			else
			{
				Body = GetNestedBodyParts(raw.Payload.Parts.ToList(), "");
			}
		}

		/// <summary>
		/// Decode a Base64 string of a message
		/// </summary>
		/// <param name="s">Base64 message part</param>
		/// <returns></returns>
		private static string DecodeBase64String(string s)
		{
			var ts = s.Replace("-", "+");
			ts = ts.Replace("_", "/");
			var bc = Convert.FromBase64String(ts);
			var tts = Encoding.UTF8.GetString(bc);

			return tts;
		}

		//Maximum message size for parsing
		private const int MAX_MESSAGE_SIZE = 200;

		/// <summary>
		/// Get nested body parts of all message parts given
		/// </summary>
		/// <param name="part">Mesasge parts to parse</param>
		/// <param name="curr">parsed message so far</param>
		/// <returns></returns>
		private static string GetNestedBodyParts(List<MessagePart> part, string curr)
		{
			string str = curr;
			if (part == null)
			{
				return str;
			}
			else
			{
				foreach (MessagePart parts in part)
				{
					if (parts.Parts == null)
					{
						if (parts.Body != null && parts.Body.Data != null && parts.MimeType == "text/plain")
						{
							var ts = DecodeBase64String(parts.Body.Data);
							str += ts;
							if (str.Length > MAX_MESSAGE_SIZE)
								return str;
						}
					}
					else
					{
						return GetNestedBodyParts(parts.Parts.ToList(), str);
					}
				}

				return str;
			}
		}
		#endregion

		/// <summary>
		/// Delete this message
		/// </summary>
		public void Delete()
		{
			using var client = new GmailClient();
			client.DeleteEmail(this);
		}

		/// <summary>
		/// Send a message.
		/// </summary>
		/// <param name="to">Email address to send the message to</param>
		public void Send(string to, string replyToId = null)
		{
			using GmailClient client = new GmailClient();
			client.SendEmail(this, to, replyToId);
		}
	}
}
