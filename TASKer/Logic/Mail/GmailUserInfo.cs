﻿namespace TASKer.Logic.Mail
{
	public class GmailUserInfo
	{

		private static GmailUserInfo current;
		public static GmailUserInfo Current
		{
			get
			{
				if (current == null) current = new GmailUserInfo(null);
				return current;
			}
			set
			{
				current = value;
			}
		}

		public string Address { get; set; }

		private GmailUserInfo(string address)
		{
			Address = address;
		}

	}
}
