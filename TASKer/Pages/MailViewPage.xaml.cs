﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TASKer.Controls;
using TASKer.Dialogs;
using TASKer.Logic;
using TASKer.Logic.APIs;
using TASKer.Logic.Database;
using TASKer.Logic.Mail;
using TASKer.Logic.Mail.Constraint;

namespace TASKer.Pages
{
	/// <summary>
	/// Interaction logic for MailViewPage.xaml
	/// 
	/// This page is used for mail interactions. It contains a list of the user created mail groups,
	/// and actions to manage them, a list of messages per mail group, and a large mail view for
	/// mail messages, with actions for interacting with them.
	/// </summary>
	public partial class MailViewPage : Page
	{

		public const int MESSAGES_PER_PAGE = 10;

		//Messages loaded for right now.
		private List<MailMessage> msgs;
		//Mail groups created by the user
		private readonly List<MailGroup> groups;

		//Current message presented
		private MailMessage currentMsg = null;
		//Current group selected
		private MailGroup currentGroup = null;
		//Current button of the group selected (for deletion).
		private Button currentGroupButton;

		//Selected mail page
		private int currentMailpage = 0;

		/// <summary>
		/// Create a new MailViewPage
		/// </summary>
		public MailViewPage()
		{
			InitializeComponent();
			groups = new List<MailGroup>();

			//Load the mail in a different thread.
			new Thread(Initialize).Start();
			groupsPanel.Children.Add(new TextBlock() { Text = "Loading mail..." });
		}

		/// <summary>
		/// Inititalize groups and messages
		/// </summary>
		public void Initialize()
		{
			//Get messages
			msgs = MailRetreival.GetMessages(MESSAGES_PER_PAGE, currentMailpage);

			//Get groups
			groups.Clear();
			groups.AddRange(MailRetreival.GetAllMailGroups(msgs));

			//Draw groups
			DrawGroups();
		}

		/// <summary>
		/// Draw / update the groups list
		/// </summary>
		public void DrawGroups()
		{
			//Run on a dispatcher because its on a different thread
			groupsPanel.Dispatcher.Invoke(() =>
			{
				groupsPanel.Children.Clear();
				foreach (MailGroup group in groups)
				{
					Button b = new Button()
					{
						Content = group.Name,
						Style = FindResource("ButtonHoverEffect") as Style,
						FontSize = 16,
					};
					b.Click += (s, e) =>
					{
						SelectGroup(group, b);
						currentMailpage = 0;
					};
					groupsPanel.Children.Add(b);
				}
			});

		}

		/// <summary>
		/// Select and draw the messages of the given group
		/// </summary>
		/// <param name="group">group to select</param>
		/// <param name="origin">Button that caused it (to delete if the group is deleted)</param>
		private void SelectGroup(MailGroup group, Button origin)
		{
			currentGroup = group;
			currentGroupButton = origin;

			//Clear messages panel from messages of the previously selected group
			mailPanel.Children.Clear();

			GroupTitle.Content = group.Name;

			Button delete = new Button()
			{
				Content = "Delete group",
				Style = FindResource("ButtonHoverEffect") as Style,
				FontSize = 18,
			};
			delete.Click += (s, e) =>
			{

				//Delete the constraint
				new DbMailConstraint(group.Name, group.Constraint.Encode()).Delete();

				(origin.Parent as StackPanel).Children.Remove(origin);
				GroupTitle.Content = "Selected Group";
				mailPanel.Children.Clear();
				groups.Remove(group);
			};
			mailPanel.Children.Add(delete);

			if (group.Messages.Count == 0)
			{
				mailPanel.Children.Add(new TextBlock()
				{
					Text = "No messages...",
					Foreground = new SolidColorBrush(Colors.White),
				});
			}
			foreach (MailMessage message in group.Messages)
			{
				SmallMailView view = new SmallMailView(mailPanel, message);
				view.Margin = new Thickness(0, 15, 0, 0);
				view.Click += (s, e) =>
				{
					SelectMessage(message);
				};
				mailPanel.Children.Add(view);
			}
		}


		/// <summary>
		/// Select a message to view in the message view panel
		/// </summary>
		/// <param name="message">Message to view</param>
		private void SelectMessage(MailMessage message)
		{
			//Clear previously viewed message
			messagePanel.Children.Clear();

			//Load the full message body
			message.Load();

			//Show the message
			currentMsg = message;
			SenderLabel.Content = currentMsg.Sender;
			foreach (MailMessage m in message.Conversation.Messages)
			{
				m.Load();
				TextBlock tb = new TextBlock { Text = m.Body, Width = 300, Background = new SolidColorBrush(Colors.White), HorizontalAlignment = HorizontalAlignment.Left };
				tb.Margin = new Thickness(30, 10, 0, 0);
				if ("אבגדהוזחטיכלמנסעפצקרשת".Any(m.Body.Contains))
				{
					tb.FlowDirection = FlowDirection.RightToLeft;
				}
				else
				{
					tb.FlowDirection = FlowDirection.LeftToRight;
				}
				tb.TextWrapping = TextWrapping.Wrap;
				messagePanel.Children.Add(tb);
			}

		}

		/// <summary>
		/// Add a group to the group list with the given constraint
		/// </summary>
		/// <param name="name">Group name to add</param>
		/// <param name="constraint">Constraint for the group</param>
		public void AddGroup(string name, MailConstraint constraint)
		{
			groups.Add(new MailGroup(name, msgs, constraint));
			DrawGroups();
		}

		/// <summary>
		/// Called when the send reply button is clicked.
		/// Sends the reply text as a reply to the currently selected message.
		/// </summary>
		private void SendReply_Click(object sender, RoutedEventArgs e)
		{
			if (currentMsg == null) return;
			new MailMessage("A reply from TASKer", "me", DateTime.Now, ReplyText.Text).Send(currentMsg.Sender, currentMsg.Id);
		}

		/// <summary>
		/// Called when the RemindMe button is clicked.
		/// Opens a dialog for creating a UserTask from this email.
		/// </summary>
		private void RemindMe_Click(object sender, RoutedEventArgs e)
		{
			if (currentMsg == null) return;
			TaskEditDialog popup = new TaskEditDialog(null, new UserTask("Reply to mail \"" + currentMsg.Subject + "\"", DateTime.Now.Date, 0, Guid.Empty, 0));
			popup.ShowDialog();
		}

		/// <summary>
		/// Called when the delete button is clicked.
		///	Delets the currently viewed message.
		/// </summary>
		private void Delete_Click(object sender, RoutedEventArgs e)
		{
			if (currentMsg == null) return;
			currentMsg.Delete();
			SenderLabel.Content = "Sender / Participants";
			messagePanel.Children.Clear();
			currentMsg = null;
		}

		/// <summary>
		/// Called when the new message button is clicked. Creates a dialog for composing a new email message.
		/// </summary>
		private void SendMessage_Click(object sender, RoutedEventArgs e)
		{
			new MessageComposeDialog().ShowDialog();
		}

		/// <summary>
		/// Called when the new mail group button is clicked.
		/// Opens a dialog for creating a new mail group.
		/// </summary>
		private void NewGroup_Click(object sender, RoutedEventArgs e)
		{
			MailGroupEditDialog popup = new MailGroupEditDialog(DrawGroups);
			popup.ShowDialog();
		}

		/// <summary>
		/// Update the messages when a page is changed.
		/// </summary>
		private void UpdatePageMessages()
		{

			new Thread(() =>
			{
				groupsPanel.Dispatcher.Invoke(() =>
				{
					mailPanel.Children.Clear();
					mailPanel.Children.Add(new TextBlock() { Text = "Loading mail...", Foreground = new SolidColorBrush(Colors.White), FontSize=30 });
				});
				msgs = MailRetreival.GetMessages(MESSAGES_PER_PAGE, currentMailpage);
				MailGroup pageGroup = MailRetreival.GetGroupByName(currentGroup.Name, msgs);
				groupsPanel.Dispatcher.Invoke(() =>
				{
					mailPanel.Children.Clear();
					SelectGroup(pageGroup, currentGroupButton);
				});
			}).Start();

		}

		/// <summary>
		/// Called when the next page button is clicked.
		/// Loads older mails from the next page.
		/// </summary>
		private void NextPage_Click(object sender, RoutedEventArgs e)
		{
			currentMailpage++;
			UpdatePageMessages();
		}

		/// <summary>
		/// Called when the previous page button is clicked.
		/// Loads newer mails from the previous page.
		/// </summary>
		private void PrevPage_Click(object sender, RoutedEventArgs e)
		{
			if (currentMailpage > 0)
			{
				currentMailpage--;
				UpdatePageMessages();
			}
		}
	}
}
