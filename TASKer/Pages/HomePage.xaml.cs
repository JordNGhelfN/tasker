﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TASKer.Controls;
using TASKer.Logic;
using TASKer.Logic.APIs;
using TASKer.Logic.Mail;
using TASKer.Logic.Plan;
using TASKer.Util;

namespace TASKer.Pages
{
	/// <summary>
	/// Interaction logic for HomePage.xaml
	/// 
	/// This page contains a dashboard for the 3 main applications of this program:
	/// A tasks widget, with upcoming tasks for this week,
	/// A mail widget, with recent mails,
	/// A plan widget, with current assigned task slot, upcoming slots, and actions for skipping the current assignment.
	/// </summary>
	public partial class HomePage : Page
	{

		/// <summary>
		/// Create a Homepage.
		/// </summary>
		public HomePage()
		{
			InitializeComponent();

			//Load mails in a different thread, show a loading message until thrn.
			new Thread(DrawEmails).Start();
			mainMail.Children.Add(new TextBlock() { Text = "Loading mail...", Foreground = new SolidColorBrush(Colors.White) });

			//Draw the tasks widget
			DrawTasks();

			//Draw the plan widget
			DrawPlan();
		}


		/// <summary>
		/// Draw / update the tasks widget
		/// </summary>
		public void DrawTasks()
		{
			//Get upcoming tasks this wekk
			List<UserTask> tasks = UserTaskRetreival.GetUserTasksInRange(DateTime.Now, DateTime.Now + new TimeSpan(7, 0, 0, 0));

			Dictionary<DateTime, StackPanel> days = new Dictionary<DateTime, StackPanel>();

			foreach (UserTask t in tasks)
			{
				TaskView task = new TaskView(DrawTasks, t);

				if (!days.ContainsKey(t.Time.Date))
				{
					days[t.Time.Date] = new StackPanel();
				}
				days[t.Time.Date].Children.Add(task);
			}

			upcomingTasks.Children.Clear();
			foreach (DateTime day in days.Keys.OrderBy(d => d.Date))
			{
				StackPanel dayTasks = days[day];
				Label title = new Label() { Content = day.DayOfWeek, FontSize = 20, Foreground=(SolidColorBrush)FindResource("DetailSecondary") };
				DockPanel.SetDock(title, Dock.Top);
				upcomingTasks.Children.Add(title);
				DockPanel.SetDock(dayTasks, Dock.Top);
				upcomingTasks.Children.Add(dayTasks);
			}
		}

		/// <summary>
		/// Called when new task button is clicked. Opens a dialog for creating a new UserTask.
		/// </summary>
		private void New_Task(object sender, RoutedEventArgs e)
		{
			TaskEditDialog popup = new TaskEditDialog(DrawTasks);
			popup.ShowDialog();
		}

		/// <summary>
		/// Draws / updates the email widget.
		/// </summary>
		public void DrawEmails()
		{
			using GmailClient client = new GmailClient();
			List<MailMessage> msgs = client.GetMessages(5, 0);
			//Requires dispatcher invokation because this is called on another thread.
			mainMail.Dispatcher.Invoke(() =>
			{
				mainMail.Children.Clear();
				foreach (MailMessage m in msgs)
				{
					SmallMailView view = new SmallMailView(mainMail, m);
					view.Margin = new Thickness(10);
					mainMail.Children.Add(view);
					DockPanel.SetDock(view, Dock.Top);
				}
			});
		}

		/// <summary>
		/// Draw / update the plan widget
		/// </summary>
		private void DrawPlan()
		{
			List<PlanSlot> slots = PlanSlotRetreival.GetPlanSlotsOnDayAfterTime(DateTime.Now);
			
			slots = slots.OrderBy(s => s.StartTime).ToList();
			if(slots.Count == 0)
			{
				CurrentPlannedTask.Text = "Done for today!";
				CurrentPlannedTaskTimes.Text = "";
			}
			else
			{

				if (slots[0].StartTime < DateTime.Now.TimeOfDay)
				{
					CurrentPlannedTask.Text = slots[0].Title;
					CurrentPlannedTaskTimes.Text = slots[0].StartTime.ToString(@"hh\:mm") + " - " + slots[0].EndTime.ToString(@"hh\:mm");
					slots.RemoveAt(0);
				}
				else
				{
					CurrentPlannedTask.Text = "Done for now!";
					CurrentPlannedTaskTimes.Text = "";
				}
				LaterTasks.Children.Clear();
				foreach(PlanSlot slot in slots)
				{
					TextBlock tb = new TextBlock()
					{
						Text = slot.Title + " (" + slot.StartTime.ToString(@"hh\:mm") + " - " + slot.EndTime.ToString(@"hh\:mm") + ")",
						FontSize = 14,
						Foreground = new SolidColorBrush(Colors.White),
						Margin = new Thickness(20, 10, 20, 10),
				};
					LaterTasks.Children.Add(tb);
				}
			}
		}

		/// <summary>
		/// Called when the show more button of the mail section is clicked. 
		/// Navigates to the Mail page.
		/// </summary>
		private void MailShowMore_Click(object sender, RoutedEventArgs e)
		{
			((MainWindow)Window.GetWindow(this)).MainFrame.Navigate(new MailViewPage());
		}

		/// <summary>
		/// Called when the show more button of the tasks section is clicked.
		/// Navigates to the tasks page.
		/// </summary>
		private void TasksShowMore_Click(object sender, RoutedEventArgs e)
		{
			((MainWindow)Window.GetWindow(this)).MainFrame.Navigate(new TasksPage());
		}

		/// <summary>
		/// Called when the show more button of the plan section is clicked.
		/// Navigates to the weekly plan page.
		/// </summary>
		private void PlanShowMore_Click(object sender, RoutedEventArgs e)
		{
			((MainWindow)Window.GetWindow(this)).MainFrame.Navigate(new WeekPlanPage());
			DrawPlan();
		}

		/// <summary>
		/// Called when the user clicks on the "not this task" button.
		/// Regenerates the plan with another task for the current time.
		/// </summary>
		private void NotThisButton_Click(object sender, RoutedEventArgs e)
		{
			PlanGenerator.GenerateAndPostpone(DateTime.Now, DateTime.Now.NextDayOfWeek(DayOfWeek.Saturday));
			DrawPlan();
		}

		/// <summary>
		/// Called when the user click on the "clear this slot" button.
		/// Regenerates the plan with no task for the current time.
		/// </summary>
		private void NothingButton_Click(object sender, RoutedEventArgs e)
		{
			PlanGenerator.Generate(DateTime.Now.AddHours(1), DateTime.Now.NextDayOfWeek(DayOfWeek.Saturday));
			DrawPlan();
		}
	}
}
