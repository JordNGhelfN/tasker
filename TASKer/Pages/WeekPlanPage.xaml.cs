﻿using System;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using TASKer.Controls;
using TASKer.Dialogs;
using TASKer.Logic;
using TASKer.Logic.APIs;
using TASKer.Logic.Plan;
using TASKer.Util;

namespace TASKer.Pages
{
	/// <summary>
	/// Interaction logic for WeekPlanPage.xaml
	/// 
	/// A page that views a weekly plan of assignments of tasks to times in the week.
	/// Allowed dragging to create new slots, locking slots so that they don't get changed by 
	/// the generator, and actions for regenrating.
	/// </summary>
	public partial class WeekPlanPage : Page
	{

		//Constant for the amount of pixels in an hour row in the calendar.
		private const int HOUR_HEIGHT = 60;

		//Current task being performed in the slot assigned to now, null if no slot or if taskless slot.
		private UserTask currentTask;

		//If no other dialog window is open right now, to prevent dragging when clicking on other windows.
		public bool HasFocus { get; set; } = true;

		/// <summary>
		/// Create a week plan page.
		/// </summary>
		public WeekPlanPage()
		{
			InitializeComponent();

			Loaded += delegate
			{
				InitializeTimeGuides();

				LoadPlan();
			};
		}

		/// <summary>
		/// Draw / update the calendar time guides (hour labels and lines, today highlight...)
		/// </summary>
		private void InitializeTimeGuides()
		{
			MainCanvas.Height = 24 * HOUR_HEIGHT;
			MainPanel.Height = 24 * HOUR_HEIGHT;

			MainCanvas.Width = TopLevelGrid.ActualWidth;
			MainPanel.Width = TopLevelGrid.ActualWidth;
			MainCanvas.Background = new SolidColorBrush(Colors.Transparent);
			for (int hour = 0; hour < 24; hour++)
			{
				TextBlock t = new TextBlock()
				{
					Text = hour + ":00",
					Height = HOUR_HEIGHT,
					Margin = new Thickness(0, hour * HOUR_HEIGHT, 0, 0),
					VerticalAlignment = VerticalAlignment.Top,
					Foreground = new SolidColorBrush(Colors.White),
					FontSize = 20,
				};
				MainPanel.Children.Add(t);
				Grid.SetColumn(t, 0);

				Line l = new Line()
				{
					X1 = 0,
					X2 = System.Windows.SystemParameters.PrimaryScreenWidth,
					Y1 = hour * HOUR_HEIGHT,
					Y2 = hour * HOUR_HEIGHT,
					Stroke = new SolidColorBrush(Colors.White),
					StrokeThickness = 2,
				};
				MainPanel.Children.Add(l);
				Grid.SetColumnSpan(l, 8);
			}

			Line now = new Line()
			{
				X1 = 0,
				X2 = System.Windows.SystemParameters.PrimaryScreenWidth,
				Y1 = DateTime.Now.TimeOfDay.TotalHours * HOUR_HEIGHT,
				Y2 = DateTime.Now.TimeOfDay.TotalHours * HOUR_HEIGHT,
				Stroke = new SolidColorBrush(Colors.Red),
				StrokeThickness = 2,
			};
			MainPanel.Children.Add(now);
			Grid.SetColumnSpan(now, 8);
			Grid.SetZIndex(now, 1000);

			Grid.SetColumn(TodayHighlight, GetDayColumn(DateTime.Today));

			Scroller.ScrollToVerticalOffset(DateTime.Now.TimeOfDay.TotalHours * HOUR_HEIGHT - 100);

			if (DateTime.Today.DayOfWeek == DayOfWeek.Sunday) SunDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Monday) MonDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Tuesday) TuesDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday) WednesDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Thursday) ThursDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Friday) FriDay.Foreground = new SolidColorBrush(Colors.Orange);
			if (DateTime.Today.DayOfWeek == DayOfWeek.Saturday) SaturDay.Foreground = new SolidColorBrush(Colors.Orange);
		}

		/// <summary>
		/// Load the current plan and draw the slots
		/// </summary>
		private void LoadPlan()
		{
			HasFocus = true;

			List<UIElement> childrenToRemove = new List<UIElement>();
			foreach (UIElement element in MainPanel.Children) if (element is PlanSlotView) childrenToRemove.Add(element);
			foreach (UIElement element in childrenToRemove) MainPanel.Children.Remove(element);

			DateTime[] dates = DatesUtils.GetDatesBetween(DateTime.Today.LastDayOfWeek(DayOfWeek.Sunday), DateTime.Today.NextDayOfWeek(DayOfWeek.Saturday));

			foreach (PlanSlot slot in PlanSlotRetreival.GetAllPlanSlots())
			{
				foreach (DateTime date in dates)
				{
					if (slot.RepeatOnDate(date))
						DrawSlot(slot, date);
				}
			}
		}

		/// <summary>
		/// Draw a slot in a date.
		/// </summary>
		/// <param name="slot">The slot to draw</param>
		/// <param name="date">The day to draw on</param>
		private void DrawSlot(PlanSlot slot, DateTime date)
		{
			PlanSlotView b = new PlanSlotView(slot, this)
			{
				Height = (slot.EndTime - slot.StartTime).TotalHours * HOUR_HEIGHT,
				Margin = new Thickness(0, slot.StartTime.TotalHours * HOUR_HEIGHT, 0, 0),
				VerticalAlignment = VerticalAlignment.Top,
			};
			MainPanel.Children.Add(b);
			Grid.SetColumn(b, GetDayColumn(date));

			if (date + slot.StartTime < DateTime.Now && DateTime.Now < date + slot.EndTime) currentTask = slot.UserTask;
		}

		/// <summary>
		/// Get the column number of the date
		/// </summary>
		/// <param name="time">date to find</param>
		/// <returns>The column id on the week view grid for this date</returns>
		public int GetDayColumn(DateTime time)
		{
			return (int)time.DayOfWeek + 1;
		}
		/// <summary>
		/// Get the day for a column id from the week grid
		/// </summary>
		/// <param name="column">Column to check</param>
		/// <returns>The date viewed on the column with the given id</returns>
		public DateTime GetColumnDay(int column)
		{
			return DateTime.Today.LastDayOfWeek(DayOfWeek.Sunday).AddDays(column - 1);
		}

		//Constant for the starting width of the slot creation guide.
		private const int DRAG_WIDTH = 100;

		//True if currently dragging.
		private bool dragging = false;
		//The PlanSlotView of the dragging guide
		private PlanSlotView dragGuide = null;

		/// <summary>
		/// Called when the user clicks anywhere on the week view.
		/// Starts dragging a guide for a new slot.
		/// </summary>
		private void MainCanvas_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			if (dragging) return;
			if (!HasFocus)
			{
				HasFocus = true;
				return;
			};

			dragging = true;
			TimeSpan start = TimeSpan.FromHours((Mouse.GetPosition(MainCanvas).Y - Mouse.GetPosition(MainCanvas).Y % (HOUR_HEIGHT / 2)) / HOUR_HEIGHT);
			DateTime date = GetColumnDay(MainPanel.GetCellAt(Mouse.GetPosition(MainPanel).X, Mouse.GetPosition(MainPanel).Y).col);
			PlanSlot slot = new PlanSlot(Guid.Empty, "New Slot", Guid.Empty, date, start, start + TimeSpan.FromMinutes(30), RepeatType.Never);
			dragGuide = new PlanSlotView(slot, this, true)
			{
				Width = DRAG_WIDTH,
				Height = HOUR_HEIGHT / 2,
				Margin = new Thickness(0, Mouse.GetPosition(MainCanvas).Y - Mouse.GetPosition(MainCanvas).Y % (HOUR_HEIGHT / 2), 0, 0),
				VerticalAlignment = VerticalAlignment.Top,
			};
			MainPanel.Children.Add(dragGuide);
			Grid.SetColumn(dragGuide, (int)date.DayOfWeek + 1);
		}

		/// <summary>
		/// Stop the drag, destroy the guide.
		/// </summary>
		private void StopDrag()
		{
			if (dragGuide == null) return;
			MainPanel.Children.Remove(dragGuide);
			dragGuide = null;
		}

		/// <summary>
		/// Called when the user moves the mouse.
		/// If currently dragging, update the guide.
		/// </summary>
		private void MainCanvas_MouseMove(object sender, MouseEventArgs e)
		{
			if (dragging)
			{
				if (!HasFocus) { StopDrag(); return; }
				double newHeight = Mouse.GetPosition(MainCanvas).Y - dragGuide.Margin.Top;
				if (newHeight < HOUR_HEIGHT / 2) newHeight = HOUR_HEIGHT / 2;
				dragGuide.Height = newHeight;
			}
		}

		/// <summary>
		/// Called when the user stops dragging.
		/// Stops the drag and opens a dialog for creating a new slot
		/// </summary>
		private void MainCanvas_PreviewMouseUp(object sender, MouseButtonEventArgs e)
		{
			if (!dragging) return;
			if (!HasFocus) { StopDrag(); return; }
			dragging = false;
			PlanSlot slot = dragGuide.Slot;
			slot.EndTime = slot.StartTime + TimeSpan.FromHours(dragGuide.Height / HOUR_HEIGHT);
			dragGuide = null;

			HasFocus = false;
			StopDrag();
			new PlanSlotEditDialog(LoadPlan, slot).ShowDialog();
		}

		/// <summary>
		/// Called when the generate button is clicked. Generates a nwe plan
		/// </summary>
		private void GenerateButton_Click(object sender, RoutedEventArgs e)
		{
			PlanGenerator.Generate(DateTime.Now, DateTime.Today.NextDayOfWeek(DayOfWeek.Saturday));
			LoadPlan();
		}

		/// <summary>
		/// Called when the dont want to do anything button is clicked.
		/// Generates a plan with no slot assigned to right now.
		/// </summary>
		private void NothingButton_Click(object sender, RoutedEventArgs e)
		{
			PlanGenerator.Generate(DateTime.Now.AddHours(1), DateTime.Today.NextDayOfWeek(DayOfWeek.Saturday));
			LoadPlan();
		}

		/// <summary>
		/// Called when the don't want to do this button is clicked.
		/// Generates a plan with a different slot for right now.
		/// </summary>
		private void NotThisButton_Click(object sender, RoutedEventArgs e)
		{
			PlanGenerator.GenerateAndPostpone(DateTime.Now, DateTime.Today.NextDayOfWeek(DayOfWeek.Saturday), currentTask);
			LoadPlan();
		}

		/// <summary>
		/// Called when the size of the page is changed. Updates the width of the week view grid.
		/// </summary>
		private void Page_SizeChanged(object sender, SizeChangedEventArgs e)
		{
			MainCanvas.Width = TopLevelGrid.ActualWidth;
			MainPanel.Width = TopLevelGrid.ActualWidth;
		}
	}
}
