﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using TASKer.Controls;
using TASKer.Dialogs;
using TASKer.Logic;
using TASKer.Logic.APIs;

namespace TASKer.Pages
{
	/// <summary>
	/// Interaction logic for TasksPage.xaml
	/// 
	/// This page contains interactions with tasks: A panel with task categories and actions for editing, deleting
	/// or creating new categories, a list of all tasks in the database with actions for editing or deleting,
	/// and a calendar with tasks in the days they are due to.
	/// </summary>
	public partial class TasksPage : Page
	{

		/// <summary>
		/// Creates a new Tasks page
		/// </summary>
		public TasksPage()
		{
			InitializeComponent();

			LoadCategories();
			DrawTasks();
		}

		/// <summary>
		/// Load / updates the categories.
		/// </summary>
		public void LoadCategories()
		{
			CategoriesContainer.Children.Clear();
			foreach(TaskCategory category in CategoryRetreival.GetAllCategories())
			{
				CategoriesContainer.Children.Add(new CategoryView(category, this));
			}
		}

		/// <summary>
		/// Called when the new category button is clicked.
		/// Creates a dialog for creating a category.
		/// </summary>
		private void NewCategory_Click(object sender, RoutedEventArgs e)
		{
			new CategoryEditDialog(LoadCategories).ShowDialog();
		}

		/// <summary>
		/// Draws / updates the tasks list
		/// </summary>
		public void DrawTasks()
		{
			List<UserTask> tasks = UserTaskRetreival.GetAllTasks();

			Dictionary<DateTime, StackPanel> days = new Dictionary<DateTime, StackPanel>();

			foreach (UserTask t in tasks)
			{
				TaskView task = new TaskView(DrawTasks, t);

				if (!days.ContainsKey(t.Time.Date))
				{
					days[t.Time.Date] = new StackPanel();
				}
				days[t.Time.Date].Children.Add(task);
			}

			upcomingTasks.Children.Clear();
			foreach (DateTime day in days.Keys.OrderBy(d => d.Date))
			{
				StackPanel dayTasks = days[day];
				Label title = new Label() { Content = day.Date.ToString("dd.MM") + "(" + day.DayOfWeek + ")", FontSize = 20, Foreground = (SolidColorBrush)FindResource("DetailSecondary") };
				DockPanel.SetDock(title, Dock.Top);
				upcomingTasks.Children.Add(title);
				DockPanel.SetDock(dayTasks, Dock.Top);
				upcomingTasks.Children.Add(dayTasks);
			}
		}

		/// <summary>
		/// Called when the new task button is clicked.
		/// Opens a dialog for creating a new task.
		/// </summary>
		private void New_Task(object sender, RoutedEventArgs e)
		{
			TaskEditDialog popup = new TaskEditDialog(() => { DrawTasks(); MonthView.LoadMonth(); });
			popup.ShowDialog();
		}

	}
}
