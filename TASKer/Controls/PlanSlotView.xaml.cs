﻿using System;
using System.Windows.Controls;
using System.Windows.Input;
using TASKer.Dialogs;
using TASKer.Logic.Plan;
using TASKer.Pages;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for PlanSlotView.xaml
	/// 
	/// Views a plan slot on the week plan page. 
	/// </summary>
	public partial class PlanSlotView : UserControl
	{

		//The owning page
		private WeekPlanPage page;

		//The slot that this represents
		public PlanSlot Slot { get; set; }

		//Whether this is a "guide" slot view, the view that is created when the user
		//drags on the window to create a new slot.
		private bool isGuide;

		/// <summary>
		/// Create a new PlanSlotView.
		/// </summary>
		/// <param name="slot">The slot for this view</param>
		/// <param name="page">The owning page</param>
		/// <param name="isGuide">Optional, true if this is a guide view. Default is false.</param>
		public PlanSlotView(PlanSlot slot, WeekPlanPage page, bool isGuide = false)
		{
			InitializeComponent();
			this.isGuide = isGuide;

			Slot = slot;

			SlotName.Text = slot.Title;

			Lock.Locked = slot.Locked;
			this.page = page;

		}
		
		/// <summary>
		/// Update the status of this slot (name and locked state).
		/// </summary>
		public void Update()
		{
			SlotName.Text = Slot.Title;

			Lock.Locked = Slot.Locked;
		}

		/// <summary>
		/// Called when this view is clicked. Opens a dialog for editing the slot.
		/// </summary>
		private void DockPanel_PreviewMouseDown(object sender, MouseButtonEventArgs e)
		{
			page.HasFocus = false;
			new PlanSlotEditDialog(Update, Slot).ShowDialog();
		}

		/// <summary>
		/// Called when the user locks this slot. Updates the state.
		/// </summary>
		private void Lock_OnLock(object sender, EventArgs e)
		{
			Slot.Locked = true;
		
			//The guide should never be added to the db.
			if (!isGuide) 
				Slot.Update();
		}

		/// <summary>
		/// Called when the user unlocks this slot. Updates the state.
		/// </summary>
		private void Lock_OnUnlock(object sender, EventArgs e)
		{
			Slot.Locked = false;

			//The guide should never be added to the db.
			if (!isGuide)
				Slot.Update();
		}
	}
}
