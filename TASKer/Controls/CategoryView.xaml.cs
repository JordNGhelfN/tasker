﻿using System.Windows;
using System.Windows.Controls;
using TASKer.Dialogs;
using TASKer.Logic;
using TASKer.Pages;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for CategoryView.xaml
	/// 
	/// CategoryView shows information about a category and provides interface
	/// for modifying and deleting it.
	/// </summary>
	public partial class CategoryView : UserControl
	{
		
		//The page that owns this view
		private TasksPage page;
		//The category to view
		private TaskCategory category;

		/// <summary>
		/// Create a CategoryView control for a given category
		/// </summary>
		/// <param name="category">The category to view</param>
		/// <param name="page">The page to refresh when changes are made to this category</param>
		public CategoryView(TaskCategory category, TasksPage page)
		{
			InitializeComponent();

			this.category = category;
			CategoryName.Text = category.Name;

			this.page = page;
		}

		/// <summary>
		/// Called when the user clicks the "edit" button.
		/// Opens a new CategoryEditDialog to edit this category.
		/// </summary>
		private void Edit_Click(object sender, RoutedEventArgs e)
		{
			//Create a category edit dialog.
			CategoryEditDialog category = new CategoryEditDialog(page.LoadCategories, this.category);
			category.ShowDialog();
		}

		/// <summary>
		/// Called when the Delete button is clicked.
		/// Deletes this category. Also removes the view from its container.
		/// </summary>
		private void Delete_Click(object sender, RoutedEventArgs e)
		{
			category.Delete();

			page.CategoriesContainer.Children.Remove(this);
		}
	}
}
