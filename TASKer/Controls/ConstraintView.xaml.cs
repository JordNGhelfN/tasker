﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using TASKer.Logic.Mail.Constraint;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for EditConstraint.xaml
	/// 
	/// This view allowes editing of a MailConstraint, including adding children
	/// and changing parameters.
	/// </summary>
	public partial class ConstraintView : UserControl
	{

		//private MailConstraint object that this view represents.
		private MailConstraint _constraint;

		/// <summary>
		/// public property for the edited MailConstraint.
		/// </summary>
		public MailConstraint Constraint
		{
			get
			{
				return _constraint;
			}
			set
			{
				if (parent != null)
				{
					//Delete the parent's current copy of this constraint
					parent.Constraint.Children.Remove(_constraint);
					_constraint = value;
					//Insert the new, updated, value
					parent.Constraint.Children.Add(_constraint);
				}
				else
				{
					_constraint = value;
				}
			}
		}

		//The visual parent of this constraint view.
		//That is, the ConstraintView that edits the parent constraint of
		//this view's constraint.
		private readonly ConstraintView parent;

		public ConstraintView()
		{
			InitializeComponent();

			InitializeTypes();

			//If this has no parent, its the topmost constraint view, hide the remove option
			Remove.Visibility = Visibility.Hidden;
			this.parent = null;
		}

		/// <summary>
		/// Create a constraint view for a new constraint
		/// </summary>
		/// <param name="parent">Optional parent ConstraintView.</param>
		public ConstraintView(ConstraintView parent)
		{
			InitializeComponent();

			InitializeTypes();

			//If this has no parent, its the topmost constraint view, hide the remove option
			if (parent == null) Remove.Visibility = Visibility.Hidden;
			

			this.parent = parent;
		}

		/// <summary>
		/// Initialize types in the type combobox
		/// </summary>
		private void InitializeTypes()
		{
			Type.Items.Add("And");
			Type.Items.Add("Or");
			Type.Items.Add("Not");
			Type.Items.Add("Sender");
			Type.Items.Add("Subject");

			Type.SelectedIndex = 0;
		}

		/// <summary>
		/// This is called when the type combobox is changed by the user.
		/// It loads the layout of the parameters for this constraint type.
		/// </summary>
		private void Type_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			//Clear current parameters layout
			MoreOptions.Children.Clear();

			//Keep track of the old constraint.
			MailConstraint temp = Constraint;
			switch (e.AddedItems[0])
			{
				case "And":
					Constraint = new AndMailConstraint();
					break;
				case "Or":
					Constraint = new OrMailConstraint();
					break;
				case "Not":
					Constraint = new NotMailConstraint();
					break;
				case "Sender":
					{
						Constraint = new SenderMailConstraint("Sender");
						HintedTextBoxView input = new HintedTextBoxView()
						{
							Hint = "Sender address...",
						};
						Binding senderBinding = new Binding("Param");
						senderBinding.Source = (Constraint as SenderMailConstraint);
						BindingOperations.SetBinding(input, TextBox.TextProperty, senderBinding);

						MoreOptions.Children.Add(input);
						break;
					}
				case "Subject":
					{
						Constraint = new SubjectMailConstraint(SubjectMailConstraintType.contains, "");

						HintedTextBoxView input = new HintedTextBoxView()
						{
							Hint = "Subject keyword...",
						};
						input.TextChanged += (s, e) =>
						{
							(Constraint as SubjectMailConstraint).Param = input.Text;
						};
						MoreOptions.Children.Add(input);

						ComboBox subjectType = new ComboBox()
						{
							ItemsSource = Enum.GetValues(typeof(SubjectMailConstraintType)),
						};
						subjectType.SelectedIndex = 0;
						MoreOptions.Children.Add(subjectType);

						break;
					}
			}

			//Add the children of the old constraint to the new one.
			if(temp != null) 
				Constraint.Children.AddRange(temp.Children);

			//Keep track of allowed children count
			if (Constraint.Children.Count == Constraint.AllowedChildren) AddChild.Visibility = Visibility.Hidden;
			else if (Constraint.Children.Count > Constraint.AllowedChildren)
			{
				Constraint.Children.Clear();
				Children.Children.Clear();
			}
			else AddChild.Visibility = Visibility.Visible;

		}

		/// <summary>
		/// Called when the add child button is clicked. 
		/// Adds a child ConstraintView with a matching chil Constraint to this 
		/// view's children.
		/// </summary>
		private void AddChild_Click(object sender, RoutedEventArgs e)
		{
			//Create a constraint view.
			ConstraintView edit = new ConstraintView(this)
			{
				Margin = new Thickness(10, 0, 0, 0),
				Constraint = new AndMailConstraint(),
			};

			//Add it below this one
			Children.Children.Add(edit);

			//If the limit of children is reached, hide the button.
			if (Constraint.Children.Count >= Constraint.AllowedChildren) AddChild.Visibility = Visibility.Hidden;
		}

		private void Remove_Click(object sender, RoutedEventArgs e)
		{
			//The remove button is hidden for the top level cosntraint without a parent, but check just in case.
			if (parent != null)
			{
				//Remove this from the parent's list.
				if (parent.Constraint.Children.Count == parent.Constraint.AllowedChildren)
					parent.AddChild.Visibility = Visibility.Visible;

				parent.Children.Children.Remove(this);
				parent.Constraint.Children.Remove(Constraint);
			}
		}

		/// <summary>
		/// Checks if all the parameters for this constraint and all of its children
		/// were supplied by the user.
		/// </summary>
		/// <returns>True if the constraint is valid and can be created. False if a parameter is missing.</returns>
		public bool IsValid()
		{
			//Check parameters.
			if(Constraint is SubjectMailConstraint subjectConstraint)
			{
				if (string.IsNullOrWhiteSpace(subjectConstraint.Param)) 
					return false;
			}
			if(Constraint is SenderMailConstraint senderConstraint)
			{
				if (string.IsNullOrEmpty(senderConstraint.Param)) 
					return false;
			}

			//Check children
			foreach (ConstraintView child in Children.Children) if (!child.IsValid()) return false;
			
			//All good, return true
			return true;
		}
	}
}
