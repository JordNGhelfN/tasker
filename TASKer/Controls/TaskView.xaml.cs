﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Controls;
using TASKer.Dialogs;
using TASKer.Logic;
using static TASKer.Dialogs.RefreshingDialog;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for TaskView.xaml
	/// 
	/// A view that presents a task to the user, along with its category. Also a checkbox for
	/// marking a task as done, and buttons for editing and deleting the task.
	/// </summary>
	public partial class TaskView : UserControl
	{

		//The task to view.
		public UserTask Task { get; }

		//An optional event to be called when the task is modified using this control 
		//(like when it is marked as done or deleted).
		private readonly RefreshTasksMethod refresh;

		/// <summary>
		/// Create a TaskView with the given refresh event (can be null) and task to view.
		/// </summary>
		public TaskView(RefreshTasksMethod refresh, UserTask task)
		{
			InitializeComponent();
			Title.Text = task.Name;
			if (task.Category != null) Category.Text = task.Category.Name;
			else Category.Text = "";
			Task = task;
			this.refresh = refresh;

			DoneCheckBox.IsChecked = task.Done;
		}

		/// <summary>
		/// Called when the edit button is clicked. Opens a new Edit Task dialog.
		/// </summary>
		private void Edit_Click(object sender, RoutedEventArgs e)
		{
			TaskEditDialog popup = new TaskEditDialog(refresh, Task);
			popup.ShowDialog();
		}

		/// <summary>
		/// Called when the delete button is clicked. Deletes this task.
		/// </summary>
		private void Delete_Click(object sender, RoutedEventArgs e)
		{
			Task.Delete();
			refresh?.Invoke();
		}

		/// <summary>
		/// Called when the checkbox is checked. Marks the task as done.
		/// </summary>
		private void CheckBox_Checked(object sender, RoutedEventArgs e)
		{
			Task.Done = true;
			Task.Update();
		}

		/// <summary>
		/// Called when the checkbox is unchecked. Marks the task as not done.
		/// </summary>
		private void CheckBox_Unchecked(object sender, RoutedEventArgs e)
		{
			Task.Done = false;
			Task.Update();
		}
	}
}
