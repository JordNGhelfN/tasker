﻿using System;
using System.Windows;
using System.Windows.Controls;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for HintedTextBox.xaml
	/// 
	/// A Textbox control that supports a "hint" - text that will 
	/// appear until the user types something in the box.
	/// </summary>
	public partial class HintedTextBoxView : UserControl
	{

		//Dependency property for Hint.
		public static readonly DependencyProperty HintProperty = DependencyProperty.Register("Hint", typeof(string), typeof(HintedTextBoxView));
		//The hint text for this textbox.
		public string Hint
		{
			get
			{
				return GetValue(HintProperty) as string;
			}
			set
			{
				SetValue(HintProperty, value);
			}
		}

		//Dependency property for the Text property of this control.
		public static readonly DependencyProperty TextProperty = DependencyProperty.Register("Text", typeof(string), typeof(HintedTextBoxView));
		public string Text
		{
			get
			{
				return GetValue(TextProperty) as string;
			}
			set
			{
				SetValue(TextProperty, value);
				if (TextInput.Text != value) TextInput.Text = value;
				TextChanged?.Invoke(this, EventArgs.Empty);
			}
		}

		//Event to implement from outside to do something when the textbox text
		//is changed.
		public event EventHandler TextChanged;

		/// <summary>
		/// Create a hinted text box.
		/// </summary>
		public HintedTextBoxView()
		{
			InitializeComponent();

			//Used for bindings to work.
			DataContext = this;
		}

		/// <summary>
		/// Called when the text of the textbox is changed. Changes the 
		/// actual Text property that is available outside.
		/// </summary>
		private void TextInput_TextChanged(object sender, TextChangedEventArgs e)
		{
			Text = TextInput.Text;
		}
	}
}
