﻿using System;
using System.Windows;
using System.Windows.Controls;
using TASKer.Pages;
using TASKer.Util;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for MonthTasksView.xaml
	/// 
	/// This is a calendar view for tasks. 
	/// A month is presented, like a calendar, with tasks placed in the cells of their 
	/// due dates.
	/// </summary>
	public partial class MonthTasksView : UserControl
	{
		
		//The first day of the month to view. This can be navigated with the "next month"
		//and "prev month" buttons.
		private DateTime SelectedTime;

		/// <summary>
		/// Create a month tasks view.
		/// </summary>
		public MonthTasksView()
		{
			InitializeComponent();

			SelectedTime = new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
			Loaded += (s, e) =>
			{
				LoadMonth();
			};

		}

		/// <summary>
		/// Update the tasks on the page.
		/// </summary>
		public void LoadMonth()
		{
			//Clear all the day views.
			MainGrid.Children.Clear();

			int year = SelectedTime.Year;
			int month = SelectedTime.Month;

			//Draw the name of the month
			MonthTitle.Text = SelectedTime.GetMonthName() + " " + SelectedTime.Year;

			//Find the week boundries (to show complete weeks even if they overflow the month).
			DateTime lastSunday = new DateTime(year, month, 1);
			while (lastSunday.DayOfWeek != DayOfWeek.Sunday) lastSunday = lastSunday.AddDays(-1);

			DateTime firstSaturday = new DateTime(year, month, DateTime.DaysInMonth(year, month));
			while (firstSaturday.DayOfWeek != DayOfWeek.Saturday) firstSaturday = firstSaturday.AddDays(1);

			//Different layout for months with 5 and 6 weeks
			if ((firstSaturday - lastSunday).TotalDays > 5 * 7)
				if (MainGrid.RowDefinitions.Count == 5) 
					MainGrid.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(1, GridUnitType.Star) });

			if ((firstSaturday - lastSunday).TotalDays <= 5 * 7)
				if (MainGrid.RowDefinitions.Count == 6) 
					MainGrid.RowDefinitions.RemoveAt(0);

			//Draw the days. The days are responsible for drawing their tasks.
			int week = 0;
			for (DateTime day = lastSunday; day <= firstSaturday; day = day.AddDays(1))
			{
				if (day.DayOfWeek == DayOfWeek.Sunday) week++;

				MonthTasksDayView dayView = new MonthTasksDayView(day, this.FindParent<TasksPage>());
				MainGrid.Children.Add(dayView);
				Grid.SetRow(dayView, week - 1);
				Grid.SetColumn(dayView, (int)day.DayOfWeek);
			}
		}

		/// <summary>
		/// Called when the PrevMonth button is called. Changes the view to look at the 
		/// previous month.
		/// </summary>
		private void PrevMonth_Click(object sender, RoutedEventArgs e)
		{
			SelectedTime = SelectedTime.AddMonths(-1);
			LoadMonth();
		}

		/// <summary>
		/// Called when the NextMonth button is called. Changes the view to look at the 
		/// next month.
		/// </summary>
		private void NextMonth_Click(object sender, RoutedEventArgs e)
		{
			SelectedTime = SelectedTime.AddMonths(1);
			LoadMonth();
		}
	}
}
