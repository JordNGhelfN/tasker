﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using TASKer.Logic;
using TASKer.Logic.APIs;
using TASKer.Logic.Database;
using TASKer.Pages;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for MonthTasksViewDay.xaml
	/// 
	/// A view for a day of tasks to be used in a monthly calendar view.
	/// </summary>
	public partial class MonthTasksDayView : UserControl
	{

		//The date of this day
		private readonly DateTime date;

		//The parent page that owns this control.
		private TasksPage page;

		/// <summary>
		/// Create a new MonthTasksDayView for a specific date with an owner page.
		/// </summary>
		public MonthTasksDayView(DateTime date, TasksPage page)
		{
			InitializeComponent();

			DayNumber.Text = "" + date.Day;
			this.date = date;

			//Highlight today.
			if (date.Date == DateTime.Today)
			{
				MainGrid.Background = FindResource("DetailPrimary") as SolidColorBrush;
			}

			DrawTasks();

			this.page = page;
		}

		/// <summary>
		/// Draw the tasks of this day. Called to refresh the tasks.
		/// </summary>
		public void DrawTasks()
		{
			//Clear previous tasks.
			DayTasks.Children.Clear();

			//Draw the updated tasks.
			foreach (UserTask t in UserTaskRetreival.GetUserTasksInDate(date))
			{
				Button task = new Button()
				{
					Content = t.Name,
					Style = FindResource("ButtonHoverEffect") as Style,
					HorizontalAlignment = HorizontalAlignment.Left,
				};
				task.Click += (s,e) => {
					//Highlight the task clicked in the task list of this page.
					foreach (var c in page.upcomingTasks.Children)
					{
						if (c is StackPanel stack) foreach (TaskView tv in stack.Children)
							{
								if (tv.Task.Name == t.Name)
								{
									ColorAnimation anim = new ColorAnimation(Colors.Transparent, new Duration(TimeSpan.FromSeconds(0.5)));
									tv.Background = new SolidColorBrush((Color)FindResource("DetailPrimaryColor"));
									tv.Background.BeginAnimation(SolidColorBrush.ColorProperty, anim);
									tv.BringIntoView();
								}
							}
					}
				};
				DayTasks.Children.Add(task);
			}
		}
	}
}
