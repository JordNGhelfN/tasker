﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using TASKer.Logic.Mail;

namespace TASKer.Controls
{
    /// <summary>
    /// Interaction logic for SmallMailView.xaml
    /// 
    /// This views a small presentation of an email message, and allows deletion of it.
    /// It also supports a custom click event when the message is clicked.
    /// </summary>
    public partial class SmallMailView : UserControl
    {
        //The message to view
        private readonly MailMessage message;
        //The Panel that contains this message (to delete the view when the message is deleted).
        private readonly Panel container;

        //An optional click event for this view.
        public event EventHandler Click;

        /// <summary>
        /// Create a SmallMailView in the given container for the given message.
        /// </summary>
        /// <param name="container">The visual container of this view</param>
        /// <param name="message">The message to show.</param>
        public SmallMailView(Panel container, MailMessage message)
        {
            InitializeComponent();
            
            if (message == null)
            {
                throw new ArgumentNullException(nameof(message));
            }

            Title.Text = message.Subject;
            Body.Text = message.Body;
            Sender.Text = "From: " + message.Sender;
            this.message = message;
            this.container = container;
        }


        /// <summary>
        /// Called when the view is clicked. Calls the Click event if it is defined.
        /// </summary>
        private void Grid_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Click?.Invoke(sender, e);
        }

        /// <summary>
        /// Called when the user clicks the delete butotn.
        /// It deletes the mail message (moves it to trash).
        /// </summary>
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            message.Delete();
            container.Children.Remove(this);
        }
    }
}
