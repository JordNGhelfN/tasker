﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TASKer.Controls
{
	/// <summary>
	/// Interaction logic for LockToggleView.xaml
	/// 
	/// A lock toggle view is a toggle button that switches between a "locked" state
	/// and an "unlocked" state.
	/// </summary>
	public partial class LockToggleView : UserControl
	{

		//Events to implement outside for when the lock is locked or unlocked.
		public event EventHandler OnLock;
		public event EventHandler OnUnlock;

		//private field for the lock state.
		private bool locked = true;
		//Publicly available property that also calls the events.
		public bool Locked
		{
			get
			{
				return locked;
			}
			set
			{
				locked = value;
				if (value)
				{
					Toggle.Content = "🔒";
					Toggle.Foreground = new SolidColorBrush(Colors.DarkRed);
					OnLock?.Invoke(this, EventArgs.Empty);
				}
				else
				{
					Toggle.Content = "🔓";
					Toggle.Foreground = new SolidColorBrush(Colors.DarkGreen);
					OnUnlock?.Invoke(this, EventArgs.Empty);
				}
			}
		}

		/// <summary>
		/// Create a LockToggleView.
		/// </summary>
		public LockToggleView()
		{
			InitializeComponent();
		}

		/// <summary>
		/// Called when the button is clicked. Toggles the state of the lock.
		/// </summary>
		private void Toggle_Click(object sender, RoutedEventArgs e)
		{
			if (Locked) Locked = false;
			else Locked = true;
		}
	}
}
